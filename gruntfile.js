/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-05-30T16:32:22+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2017-12-08T14:21:19+01:00
 */



/*global module:false*/
module.exports = function(grunt) {

  var path = require('path');

  require(path.join(process.cwd(), 'node_modules/mtlg-gameframe/devGrunt/initConfig.js'))(grunt);
  //require(path.join(process.cwd(), '../../mtlg-gameframe/devGrunt/initConfig.js'))(grunt);

  require(path.join(process.cwd(), 'node_modules/mtlg-gameframe/devGrunt/loadNPMTasks.js'))(grunt);
  require(path.join(process.cwd(), 'node_modules/mtlg-gameframe/devGrunt/registerTasks.js'))(grunt);


};
