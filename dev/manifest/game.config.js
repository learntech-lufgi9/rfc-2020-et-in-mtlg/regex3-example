/**
 * @Author: thiemo
 * @Date:   2017-10-17T08:56:31+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-07-18T10:21:37+02:00
 */

MTLG.loadOptions({
  'width': 1920, // game width in pixels
  'height': 1080, // game height in pixels
  'languages': ['en', 'de'], // Supported languages. First language should be the most complete.
  'countdown': 180, // idle time countdown
  'fps': '60', // Frames per second
  'playernumber': 4, // Maximum number of supported players
  'FilterTouches': true, // Tangible setting: true means a tangible is recognized as one touch event. False: 4 events.
  'responsive': 0, // if set to 1 all canvas always fit to screen size
  'webgl': false, // Using webgl enables using more graphical effects
  // 'dd': {
  //   host: 'localhost',
  //   port: 3125
  // },
  'remoteteacher': {
    'teacherDefaultScreen': false
  },
  "eyetracking": {
    host: "localhost",
    port: 5000
  }
})
