/**
 * @Author: thiemo
 * @Date:   2017-10-17T08:56:31+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2017-10-19T17:01:55+02:00
 */

MTLG.loadOptions({
  'zoll': 27, // screen size in inches
  'mockLogin': true, // set to true to skip login procedure
  'mockNumber': 3, // number of generated fake logins if mockLogin is true
  'loginMode': 6, // Determines the login procedure for userLogin
  'language': 'en' // Highest priority language setting, device dependant
})
