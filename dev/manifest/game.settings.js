/**
 * @Author: thiemo
 * @Date:   2017-11-17T12:52:37+01:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-08-27T15:24:40+02:00
 */

MTLG.loadSettings({
  default: {
    tangibleIDs: ['tabula0_05', 'tabula0_06', 'tabula0_0B', 'tabula0_0C'],
    groupNumber: 40,
    playerNumber: 4,
    nbrWords: 20,
    dataLogging: 'off',
    timerMaxTime: 1 * 90 * 1000, // Per Game max time in milli-seconds
    delayWords: 5, // delay till the words will appear
    maxLevel: 15,
    plusScore: 2,
    minusScore: -1,
    wordAlignement: 'playerPosition',
    pictures: 'text'
  },
  all: {
    timerMaxTimeMin: 30,
    timerMaxTimeMax: 600,
    delayWordsMin: 0,
    delayWordsMax: 300,
    playerMax: 4, // 10
    playerMin: 1,
    wordsMax: 50, // ?
    wordsMin: 2, // 2
    plusScore: 2,
    minusScore: -1,
    maxLevelMax: 15,
    maxLevelMin: 1,
    minGroupNumber: 40,
    wordView: [{
      key: 'pictures',
      note: ''
    }, {
      key: 'text',
      note: ''
    }],
    wordAlignement: [{
      key: 'middle',
      note: ''
    },
    {
      key: 'playerPosition',
      note: ''
    },
    {
      key: 'pullingDirection',
      note: ''
    }
    ],
    dataLogging: [{
      key: 'off',
      note: 'Data is not logged remotely'
    }, {
      key: 'on',
      note: 'Interaction data is logged to LCDM server'
    }],
    games: [
      {
        key: 'Animal Game',
        note: 'Test game for picture pattern matching.'
      },
      {
        key: 'Word Game',
        note: 'Test game for word pattern matching.'
      },
      {
        key: 'RegEx',
        note: 'Game to learn pattern matching with regular expressions.'
      },
      {
        key: 'Chemie Game',
        note: 'Test game for picture pattern matching.'
      }
    ]
  }
})
