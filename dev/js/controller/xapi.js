
var xAPI = (function () { // eslint-disable-line no-unused-vars
  function init () {
    // init xapi module
    console.log('XAPI Module init')

    ADL.XAPIWrapper.changeConfig({
      'endpoint': 'https://www.tleonhardt.de/data/xAPI/',
      'user': '308bcae2a2c5309294f30a5f38cacbb44bb46ddc',
      'password': 'fb067b388bbcf0ca1607845e7e0573600f5fbfb4'
    })

    console.log(ADL.XAPIWrapper.testConfig())

    // ADL.XAPIWrapper.getStatements(null, null, getData)
    //
    // function getData (r) {
    //   let t = JSON.parse(r.response).statements
    //   console.log(t)
    // }
  }

  this.preset = function (presets) {
    console.log('presets?')
  }

  function _generateAgentId (player) {
    return 'mailto:' + player.toLowerCase().replace(/\s/g, '.') + '@tabula.de'
  }

  function _sendEvt (stmt) {
    return new Promise(function (resolve, reject) {
    // console.log(stmt)
      ADL.XAPIWrapper.sendStatement(stmt, function () {})
    })
  }

  function _sendUndefEvt (evt, playerID) {
  }

  function startSession (player) {
    let obj = new ADL.XAPIStatement.Activity('http://tabula-content.informatik.rwth-aachen.de/games/dem1-RegEx3/', 'Roadshow Demonstrator Tabula Projekt')
    obj.definition.extensions = {
      'http://tabula-content.informatik.rwth-aachen.de/games/dem1-RegEx3/sessionId': sessionId.toString(),
      'http://tabula-content.informatik.rwth-aachen.de/games/dem1-RegEx3/game': MTLG.getSettings().default.game
    }
    let verb = {
      'id': 'http://adlnet.gov/expapi/verbs/started',
      'display': { 'de-DE': 'startete',
        'en-US': 'started'
      }
    }

    let stmt = new ADL.XAPIStatement(
      new ADL.XAPIStatement.Agent(ADL.XAPIWrapper.hash(_generateAgentId(player)), player),
      verb,
      obj
    )

    _sendEvt(stmt)
  }

  function undeftouchEvent (evtData, player) {
    _sendUndefEvt()
  }

  function wordEvent (evtData, playerID) {
    // let tangibleId = ''
    // if (evtData.tangible) {
    //   tangibleId = evtData.tangible.tangibleAlias
    // }
    //
    // let ents = {
    //   'success': evtData.value.toString(),
    //   'PIDguessed': 'true',
    //   'word': evtData.word.word.label.text,
    //   'regex': evtData.area.children[2].children[1].text,
    //   'startTimestamp': evtData.word.startTimestamp.toString(),
    //   'endTimestamp': evtData.word.endTimestamp.toString(),
    //   'startX': evtData.word.startX.toString(),
    //   'startY': evtData.word.startY.toString(),
    //   'endX': evtData.word.endX.toString(),
    //   'endY': evtData.word.endY.toString(),
    //   'moveCoord': JSON.stringify(evtData.word.moveCoord),
    //   'moveDiff': JSON.stringify(evtData.word.moveDiff),
    //   'duration': (evtData.word.endTimestamp - evtData.word.startTimestamp).toString(),
    //   'groupNumber': MTLG.getSettings().default.groupNumber.toString(),
    //   'playerArea': evtData.area.aId.toString()
    // }
    let playerName
    if (playerID === undefined) {
      playerName = MTLG.getPlayerName(evtData.area.aId)
    } else {
      playerName = MTLG.getPlayerName(playerID)
    }

    let obj = new ADL.XAPIStatement.Activity('http://tabula-content.informatik.rwth-aachen.de/games/dem1-RegEx3/', 'Roadshow Demonstrator Tabula Projekt')
    obj.definition.extensions = {
      'http://tabula-content.informatik.rwth-aachen.de/games/dem1-RegEx3/sessionId': sessionId.toString()
    }
    let verb = {
      'id': 'http://adlnet.gov/expapi/verbs/scored',
      'display': { 'de-DE': 'punktete',
        'en-US': 'scored'
      }
    }
    let result = {
      'score': {
        'raw': evtData.value ? evtData.gameState.plusScore : evtData.gameState.minusScore
      },
      'success': evtData.value,
      // 'duration': 'P1DT12H',
      'extensions': {
        'http://tabula-content.informatik.rwth-aachen.de/games/dem1-RegEx3/game': MTLG.getSettings().default.game,
        'http://tabula-content.informatik.rwth-aachen.de/games/dem1-RegEx3/level': evtData.gameState.level,
        'http://tabula-content.informatik.rwth-aachen.de/games/dem1-RegEx3/word': evtData.word.word.label.text,
        'http://tabula-content.informatik.rwth-aachen.de/games/dem1-RegEx3/regex': evtData.area.children[2].children[1].text
      }
    }

    let stmt = new ADL.XAPIStatement(
      new ADL.XAPIStatement.Agent(ADL.XAPIWrapper.hash(_generateAgentId(playerName)), playerName),
      verb,
      obj
    )
    stmt.result = result

    _sendEvt(stmt)
  }

  return {
    init,
    startSession,
    wordEvent,
    undeftouchEvent
  }
})()
