/**
 * @Author: thiemo
 * @Date:   2017-10-17T11:47:11+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-09-14T17:01:08+02:00
 */

var lcdmC = (function () { // eslint-disable-line no-unused-vars
  // var johndoe

  var eventTemplate = {

    'action': 'UPDATE',
    'session': '',
    'platform': 'MOBILE',
    'category': {
      'type': 'ACADEMIC',
      'minor': ''
    },
    'entities': [{
      'key': 'lat',
      'value': '50.778474'
    },
    {
      'key': 'lng',
      'value': '6.060848'
    }
    ],
    'interests': ['Learning']

  }

  // const catMinors = new Set(['TOUCHINTERACTION', 'APPSTART'])

  this.init = function () {
    if (!MTLG.getSettings().default.dataLogging) {
      console.log('Data logging is off. Nothing to do.')
      return
    }

    MTLG.datacollector.authenticateUser('John Doe', 'Passwort', (e, r) => {
      MTLG.setAnonToken(r)
    })
  }

  this.preset = function (presets) {

  }

  function _sendEvt (evt, playerID) {
    if (!MTLG.getSettings().default.dataLogging) {
      console.log('Data logging is off. Nothing to do.')
      return
    }

    try {
      MTLG.datacollector.storeEvents([evt], MTLG.getToken(playerID), function () {
        // 20
      }, function (statusCode, response) {})
    } catch (e) {
      console.log(e)
    } finally {

    }
  };

  function _sendUndefEvt (evt, playerID) {
    if (!MTLG.getSettings().default.dataLogging) {
      console.log('Data logging is off. Nothing to do.')
      return
    }

    try {
      MTLG.datacollector.storeEvents([evt], MTLG.getAnonToken(), function () {
        // 20
      }, function (statusCode, response) {
      })
    } catch (e) {
      console.log(e)
    } finally {

    }
  };

  this.startSession = function () {
    eventTemplate.session = sessionId.toString()
    let evt = Object.assign(eventTemplate)
    evt.category.minor = 'APPSTART'
    evt.entities = [{
      'key': 'app',
      'value': 'TABULA-regex-2'
    },
    {
      'key': 'startTime',
      'value': Date.now().toString()
    },
    {
      'key': 'Session Settings',
      'value': JSON.stringify(MTLG.getSettings().default)
    },
    {
      'key': 'Options',
      'value': JSON.stringify(MTLG.getOptions())
    }
    ]
    _sendEvt(evt, 0)
    _sendEvt(evt, 1)
    _sendEvt(evt, 2)
    _sendEvt(evt, 3)
  }

  // TODO gameMode evtData lcdm update
  this.undeftouchEvent = function (evtData, player) {
    let evt = Object.assign(eventTemplate)
    evt.category.minor = 'TOUCHEVENT'

    // get tangible ID
    let tangibleId = ''
    if (evtData.tangible) {
      tangibleId = evtData.tangible.tangibleAlias
    }

    // no circuits in newCards
    let newCards = []
    evtData.gameState.wordCards.forEach(
      function (word) {
        let _newWord = {}
        _newWord.isActive = word.isActive
        _newWord.isMatched = word.isMatched
        _newWord.word = word.word
        _newWord.x = word.shape.x
        _newWord.y = word.shape.y
        newCards.push(_newWord)
      }
    )

    let ents = {
      'unspecified': 'true',
      'PIDguessed': 'false',
      'word': evtData.word.word.label.text,
      'startTimestamp': evtData.word.startTimestamp.toString(),
      'endTimestamp': evtData.word.endTimestamp.toString(),
      'startX': evtData.word.startX.toString(),
      'startY': evtData.word.startY.toString(),
      'endX': evtData.word.endX.toString(),
      'endY': evtData.word.endY.toString(),
      'moveCoord': JSON.stringify(evtData.word.moveCoord),
      'moveDiff': JSON.stringify(evtData.word.moveDiff),
      'duration': (evtData.word.endTimestamp - evtData.word.startTimestamp).toString(),
      'groupNumber': MTLG.getSettings().default.groupNumber.toString(),
      'app': 'TABULA-regex-2',
      'dataVersion': '1.6',
      // new gameState entities dataversion 1.6
      'alphabet': evtData.gameState.alphabet.toString(),
      'delayWords': evtData.gameState.delayWords.toString(),
      'endCondition': evtData.gameState.endCondition.toString(),
      'gameMode': evtData.gameState.gameMode.toString(),
      'level': evtData.gameState.level.toString(),
      'maxLevel': evtData.gameState.maxLevel.toString(),
      'timerMaxTime': evtData.gameState.timerMaxTime.toString(),
      'minusScore': evtData.gameState.minusScore.toString(),
      'nbrWords': evtData.gameState.nbrWords.toString(),
      'wordCards': JSON.stringify(newCards),
      'playerNumber': evtData.gameState.playerNumber.toString(),
      'players': JSON.stringify(evtData.gameState.players),
      'plusScore': evtData.gameState.plusScore.toString(),
      'regExLevel': JSON.stringify(evtData.gameState.regEx),
      'regExArr': JSON.stringify(evtData.gameState.regExArr),
      'regExType': evtData.gameState.regExType.toString(),
      'scoreType': evtData.gameState.scoreType.toString(),
      'useConstant': evtData.gameState.useConstant.toString(),
      'wordsArr': JSON.stringify(evtData.gameState.wordsArr),
      // tangible entity
      'tangibleId': tangibleId
    }

    let entities = []
    for (let i in ents) {
      entities.push({
        'key': i,
        'value': ents[i]
      })
    }
    evt.entities = entities
    _sendUndefEvt(evt)
  }
  // TODO gameMode evtData lcdm update

  this.touchEvent = function (evtData, player) {
    let evt = Object.assign(eventTemplate)
    evt.category.minor = 'TOUCHEVENT'

    let tangibleId = ''
    if (evtData.tangible) {
      tangibleId = evtData.tangible.tangibleAlias
    }

    // no circuits in newCards
    let newCards = []
    evtData.gameState.wordCards.forEach(
      function (word) {
        let _newWord = {}
        _newWord.isActive = word.isActive
        _newWord.isMatched = word.isMatched
        _newWord.word = word.word
        _newWord.x = word.shape.x
        _newWord.y = word.shape.y
        newCards.push(_newWord)
      }
    )

    let ents = {
      'success': evtData.value.toString(),
      'PIDguessed': 'true',
      'word': evtData.word.word.label.text,
      'regex': evtData.area.children[2].children[1].text,
      'startTimestamp': evtData.word.startTimestamp.toString(),
      'endTimestamp': evtData.word.endTimestamp.toString(),
      'startX': evtData.word.startX.toString(),
      'startY': evtData.word.startY.toString(),
      'endX': evtData.word.endX.toString(),
      'endY': evtData.word.endY.toString(),
      'moveCoord': JSON.stringify(evtData.word.moveCoord),
      'moveDiff': JSON.stringify(evtData.word.moveDiff),
      'duration': (evtData.word.endTimestamp - evtData.word.startTimestamp).toString(),
      'groupNumber': MTLG.getSettings().default.groupNumber.toString(),
      'playerArea': evtData.area.aId.toString(),
      'app': 'TABULA-regex-2',
      'dataVersion': '1.6',
      // new gameState entities dataversion 1.6
      'alphabet': evtData.gameState.alphabet.toString(),
      'delayWords': evtData.gameState.delayWords.toString(),
      'endCondition': evtData.gameState.endCondition.toString(),
      'gameMode': evtData.gameState.gameMode.toString(),
      'level': evtData.gameState.level.toString(),
      'maxLevel': evtData.gameState.maxLevel.toString(),
      'timerMaxTime': evtData.gameState.timerMaxTime.toString(),
      'minusScore': evtData.gameState.minusScore.toString(),
      'nbrWords': evtData.gameState.nbrWords.toString(),
      'wordCards': JSON.stringify(newCards),
      'playerNumber': evtData.gameState.playerNumber.toString(),
      'players': JSON.stringify(evtData.gameState.players),
      'plusScore': evtData.gameState.plusScore.toString(),
      'regExLevel': JSON.stringify(evtData.gameState.regEx),
      'regExArr': JSON.stringify(evtData.gameState.regExArr),
      'regExType': evtData.gameState.regExType.toString(),
      'scoreType': evtData.gameState.scoreType.toString(),
      'useConstant': evtData.gameState.useConstant.toString(),
      'wordsArr': JSON.stringify(evtData.gameState.wordsArr),
      // tangible entity
      'tangibleId': tangibleId
    }
    if (player === undefined) {
      player = evtData.area.aId
    }

    let entities = []
    for (let i in ents) {
      entities.push({
        'key': i,
        'value': ents[i]
      })
    }
    evt.entities = entities
    _sendEvt(evt, player)
  }

  return {
    init: this.init,
    startSession: this.startSession,
    touchEvent: this.touchEvent,
    undeftouchEvent: this.undeftouchEvent
  }
})()
