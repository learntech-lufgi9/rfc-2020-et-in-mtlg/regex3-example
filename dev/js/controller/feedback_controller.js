/**
 * @Author: thiemo
 * @Date:   2017-10-17T11:47:11+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-08-29T19:57:32+02:00
 */

var fbC = (function () { // eslint-disable-line no-unused-vars
  var types = {
    'color': 1,
    'aura': 2,
    'shake': 4,
    'sound': 8,
    'colorArea': 16,
    'turnAround': 32
  }
  var posttypes = {
    'fly': 1,
    'disappear': 2
  }
  var _presets = { // Later: Fetch from manifest/worker/...
    posColor: 'green',
    negColor: 'red',
    duration: 1000,
    delay: 0,
    intensity: 1
  }

  this.init = function (presets) {
    let prop
    for (prop in presets) {
      _presets[prop] = presets[prop] || _presets[prop]
    }
    this.preset(presets)
    this.types = types
  }

  this.preset = function (presets) {

  }

  this.getFeedback = function (action, player) {
    let isPos = action.value
    let suffix = (isPos ? 'pos' : 'neg')
    let color = (isPos ? 'green' : 'red')
    let fbOptionsArray = [{
      type: types.color + types.aura,
      posttype: 0,
      intensity: 1,
      delay: 0,
      duration: 1000,
      category: null,
      params: {
        'soundfile': 'feedback/word_' + suffix,
        'color': color,
        'isPositive': isPos
      },
      tangibleFb: {
        tcolor: color,
        sound: false,
        soundDelay: 0,
        soundDuration: 0,
        vibrate: false,
        vibrateDelay: 0,
        vibrateDuration: 0,
        light: false,
        lightDelay: 0,
        lightDuration: 0,
        tangibleAlias: ''
      }
    }]

    if (action.tangible) {
      let nativeTangibleID = action.tangible.tangibleAlias
      fbOptionsArray[0].tangibleFb.tangibleAlias = nativeTangibleID
    } else {
      if (player === undefined) {
        fbOptionsArray[0].tangibleFb.tangibleAlias = MTLG.getSettings().default.tangibleIDs[action.area.aId]
      }
    }

    fbOptionsArray[0].tangibleFb.sound = true
    fbOptionsArray[0].tangibleFb.vibrate = true
    fbOptionsArray[0].tangibleFb.light = true

    if (!isPos) {
      fbOptionsArray[0].tangibleFb.soundDuration = 1000
      fbOptionsArray[0].tangibleFb.vibrateDuration = 1000
      fbOptionsArray[0].tangibleFb.lightDuration = 1000
    } else {
      fbOptionsArray[0].tangibleFb.soundDuration = 20
      fbOptionsArray[0].tangibleFb.vibrateDuration = 20
      fbOptionsArray[0].tangibleFb.lightDuration = 1000
    }

    // TODO handle status and post type
    if (!isPos) {
      fbOptionsArray[0].type += types.shake
      fbOptionsArray[0].posttype += posttypes.fly
    } else {
      fbOptionsArray[0].type += types.turnAround
      fbOptionsArray[0].posttype += posttypes.disappear
    }

    return fbOptionsArray
  }

  return {
    init: this.init,
    getFeedback: this.getFeedback,
    preset: this.preset
  }
})()
