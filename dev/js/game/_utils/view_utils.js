/**
 * @Author: thiemo
 * @Date:   2017-10-19T09:20:01+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-09-03T21:08:16+02:00
 */

function rotate (container) {
  switch (MTLG.getSettings().default.wordAlignement) {
    case 'middle':
      rotationMiddle(container)
      break
    case 'pullingDirection':
      movingDirection(container)
      break
    case 'playerPosition':
      playerPos(container)
      break
    default:
      console.warn('No word rotation selected.')
  }
}

function calcRotation (container, point) {
  let ankathete = container.y - point.y
  let gegenkathete = container.x - point.x
  let degree

  if (container.y > point.y) {
    degree = (Math.atan(-gegenkathete / ankathete) * 180 / Math.PI) % 360
  } else {
    degree = ((Math.atan(-gegenkathete / ankathete) * 180 / Math.PI) - 180) % 360
  }

  container.rotation = degree
}

// rotate word to moddle of the screen
function rotationMiddle (container) {
  let p = {
    x: MTLG.getOptions().width / 2,
    y: MTLG.getOptions().height / 2
  }

  calcRotation(container, p)
}

// rotate word in moving direction
function movingDirection (w) {
  let pMiddle

  pMiddle = {
    x: w.startLocalX,
    y: w.startLocalY
  }

  if (outOfDelta(pMiddle, w, 20)) {
    calcRotation(w, pMiddle)
    w.startLocalX = w.x
    w.startLocalY = w.y
  }
}

function outOfDelta (pMiddle, p, delta) {
  // let dist = Math.sqrt(Math.pow(w.moveCoordLocal[w.moveCoordLocal.length - 1].x - p.x, 2) + Math.pow(w.moveCoordLocal[w.moveCoordLocal.length - 1].y - p.y, 2));
  let dist = Math.sqrt(Math.pow(pMiddle.x - p.x, 2) + Math.pow(pMiddle.y - p.y, 2))

  if (dist > 30) {
    return true
  }
  return false
}

// rotate to player position
function playerPos (w) {
  let hWidth = MTLG.getOptions().width / 2
  let hHeight = MTLG.getOptions().height / 2

  let p = { x: 0, y: 0 }

  p.x = hWidth / 2
  if (w.x > hWidth) {
    p.x += hWidth
  }

  p.y = 0
  if (w.y > hHeight) {
    p.y = MTLG.getOptions().height
  }

  calcRotation(w, p)
  w.rotation += 180
}

/// ///////////////////////////////
// standard draggable function
function draggable (e) { // eslint-disable-line no-unused-vars
  // if Text is selected set target to Container
  let target = e.target
  if (e.target.toString().slice(1, 5) === 'Text' || e.target.toString().slice(1, 5) === 'Bitm') {
    target = e.target.parent
  }

  let coords = target.localToLocal(e.localX, e.localY, MTLG.getStageContainer())
  this.x = coords.x
  this.y = coords.y

  /// ////////////////
  // verbose event logging
  let globalCoords = {
    x: e.stageX,
    y: e.stageY
  }

  this.moveCoord.push({
    x: globalCoords.x,
    y: globalCoords.y
  })
  if (this.moveCoord.length > 1) {
    this.moveDiff.push(Math.sqrt(Math.pow(globalCoords.x - this.moveCoord[this.moveCoord.length - 2].x, 2), Math.pow(globalCoords.y - this.moveCoord[this.moveCoord.length - 2].y, 2)))
  } else {
    this.moveDiff.push(Math.sqrt(Math.pow(globalCoords.x - this.startX, 2), Math.pow(globalCoords.y - this.startY, 2)))
  }
  /// //////////////////

  rotate(this)
}

function pointInBounds (pX, pY, bXmin, bYmin, bXmax, bYmax) { // eslint-disable-line no-unused-vars
  if (pX < bXmin || pX > bXmax || pY < bYmin || pY > bYmax) {
    return false
  }
  return true
}
