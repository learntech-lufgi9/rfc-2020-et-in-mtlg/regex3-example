
var gameUtils = (function () { // eslint-disable-line no-unused-vars
  function buttonNbrGroup (o, oKey, p, _container) { // eslint-disable-line no-unused-vars
    let buttons = []
    let delta = o.delta || 10
    let height = o.height || 60
    let containerGroup = new createjs.Container()
    let menuColor = o.menuColor || 'rgba(194, 45, 13, 0.85)'
    let menuTextColor = o.menuTextColor || 'rgb(0, 0, 0)'

    let key = oKey.key
    let vMax, vMin
    let steps = o.steps || 1
    let adjust = o.adjust || 1

    if (oKey.keyMax) vMax = MTLG.getSettings().all[oKey.keyMax]
    if (oKey.keyMin) vMin = MTLG.getSettings().all[oKey.keyMin]

    try {
      var b = new Button({
        text: MTLG.l(key),
        place: {
          x: 0,
          y: -40
        },
        height: 30,
        color: menuColor,
        textcolor: menuTextColor
      }, containerGroup)
    } catch (e) {
      console.warn(b)
    }

    // show number like textfield
    let textfield = new Button({
      text: MTLG.getSettings().default[key] / adjust,
      place: {
        x: 0,
        y: (height + delta)
      },
      height: 2 * height + delta
    }, containerGroup)

    buttons.push(textfield)

    let plus = new Button({
      cb: function () {
        let newValue = MTLG.getSettings().default[key] / adjust
        if (!vMax || vMax > newValue) {
          newValue += steps
        }
        this.textfield.setText(newValue)
        let _o = {
          default: {}
        }
        _o.default[key] = newValue * adjust
        MTLG.loadSettings(_o)
      },
      text: '+'
    }, containerGroup)
    buttons.push(plus)

    let minus = new Button({
      cb: function () {
        let newValue = MTLG.getSettings().default[key] / adjust
        if (vMin !== undefined && vMin < newValue) {
          newValue -= steps
        }
        this.textfield.setText(newValue)
        let _o = {
          default: {}
        }
        _o.default[key] = newValue * adjust
        MTLG.loadSettings(_o)
      },
      text: '-',
      place: {
        x: 0,
        y: (height + delta) * 3
      }
    }, containerGroup)
    buttons.push(minus)

    plus.c.textfield = textfield.c
    minus.c.textfield = textfield.c

    containerGroup.x = p.x
    containerGroup.y = p.y
    _container.addChild(containerGroup)
  }

  function buttonsSelectionGroup (o, oArr, p, _container) { // eslint-disable-line no-unused-vars
    var i, j, k, m

    let buttons = []
    let delta = o.delta || 10
    let height = o.height || 60
    let key = o.key || 'noKey'
    let def = o.default || ''
    let containerGroup = new createjs.Container()
    let menuColor = o.menuColor || 'rgba(194, 45, 13, 0.85)'
    let menuTextColor = o.menuTextColor || 'rgb(0, 0, 0)'

    let arr = []
    let arrNote = []
    for (i = 0; i < oArr.length; i++) {
      arr.push(oArr[i].key)
      let _note = oArr[i].note
      if (!_note) {
        _note = ''
      }
      arrNote.push(_note)
    }

    for (i = 0, j = arr.length; i < j; i++) {
      let _o = {
        place: {
          x: 0,
          y: 0 + i * (height + delta)
        },
        value: arr[i],
        cb: o.cb || function () {},
        cbvar: o.cbvar || 0,
        note: arrNote[i] || ''
      }
      // TODO safe o props to _o
      let b = new Button(_o, containerGroup)
      if (arr[i] === def) b.c.setColor('rgba(0, 155, 0, 0.7)')
      buttons.push(b)
    }
    for (i = 0, j = buttons.length; i < j; i++) {
      buttons[i].c.group = []
      for (k = 0, m = buttons.length; k < m; k++) {
        if (i !== k) {
          buttons[i].c.group.push(buttons[k])
        }
      }
    }
    // TODO add a pretty frame

    try {
      var b = new Button({
        text: MTLG.l(key),
        place: {
          x: 0,
          y: -40
        },
        height: 30,
        color: menuColor,
        textcolor: menuTextColor
      }, containerGroup)
    } catch (e) {
      console.warn(b)
    }

    containerGroup.x = p.x
    containerGroup.y = p.y
    _container.addChild(containerGroup)
    return buttons
  }

  function Button (o, container) {
    if (o.text === 0) {
      o.text = '0'
    }

    var prop = {
      text: o.text || MTLG.l(o.value),
      value: o.value || 0,
      place: o.place || {
        x: 0,
        y: 0
      },
      cb: o.cb || function () {},
      cbvar: o.cbvar || 0, // callback variable
      color: o.color || 'rgba(221, 205, 214, 0.85)',
      textcolor: o.textcolor || 'black',
      textstyle: o.textstyle || 'bold 20px Arial',
      width: o.width || 150,
      textSize: o.textSize || false,
      textSizeOffset: o.textSizeOffset || 10,
      height: o.height || 60,
      center: o.center || false, // set calc point to center
      note: o.note || ''
    }

    this.c = new createjs.Container()
    this.b = MTLG.utils.gfx.getShape()
    this.t = MTLG.utils.gfx.getText(prop.text, prop.textstyle, prop.textcolor)
    if (prop.textSize) {
      prop.width = this.t.getBounds().width + prop.textSizeOffset
      prop.height = this.t.getBounds().height + prop.textSizeOffset
    // prop.place.x = prop.place.x + (prop.width - 150) / 2;
    }
    this.c.addChild(this.b, this.t)

    if (prop.cbvar) this.c.cbvar = prop[prop.cbvar]

    this.setPos = function (p) {
      this.c.x = p.x
      this.c.y = p.y
    }

    this.c.setColor = function (color) {
      this.children[0].graphics.c().beginFill(color).drawRoundRect(0, 0, prop.width, prop.height, 10)
    }

    this.c.resetColor = function () {
      this.children[0].graphics.c().beginFill(prop.color).drawRoundRect(0, 0, prop.width, prop.height, 10)
    }

    this.c.resetGroupColor = function () {
      for (var i = 0; i < this.group.length; i++) {
        this.group[i].c.resetColor()
      }
    }

    this.c.setText = function (_text) {
      this.children[1].text = _text
    }

    // abstract a info screen on the side to utilities TODO
    this.c.showNote = function () {
      if (prop.note !== '') {
        let stage = MTLG.getStage().getChildAt(0) // TODO Only workinf if there is one container on stage
        if (stage.getChildByName('gamemodeNote')) {
          stage.removeChild(stage.getChildByName('gamemodeNote'))
        }

        // TODO slide it in with tween.js
        try {
          var inf = new Infoscreen({
            text: 'Description:' + '\n\n' + prop.note,
            width: 400,
            height: 200,
            place: {
              x: MTLG.getOptions().width - 420,
              y: MTLG.getOptions().height - 220
            },
            name: 'gamemodeNote'
          }, stage)
        } catch (e) {
          console.warn('Button Infoscreen: ' + inf)
        }
      }
    }

    this.c.resetColor()

    this.t.textAlign = 'center'
    this.t.textBaseline = 'middle'
    this.t.x = prop.width / 2
    this.t.y = prop.height / 2

    if (prop.center) {
      this.c.regX = prop.width / 2
      this.c.regY = prop.height / 2
    }
    // this.c.text = this.t.text;

    this.setPos(prop.place)
    this.c.on('click', prop.cb)

    if (container) {
      container.addChild(this.c)
    } else {
      return this.c
    }
  }

  function Infoscreen (o, container) {
    var prop = {
      text: o.text || MTLG.l(o.value),
      value: o.value || 0,
      place: o.place || {
        x: 0,
        y: 0
      },
      cb: o.cb || function () {},
      cbvar: o.cbvar || 0, // callback variable
      color: o.color || 'rgba(221, 205, 214, 0.85)',
      textcolor: o.textcolor || 'black',
      textstyle: o.textstyle || 'bold 20px Arial',
      width: o.width || 150,
      height: o.height || 60,
      center: o.center || false, // set calc point to center
      name: o.name || ''

    }

    this.c = new createjs.Container()
    this.b = MTLG.utils.gfx.getShape()
    this.t = MTLG.utils.gfx.getText(prop.text, prop.textstyle, prop.textcolor)
    this.c.addChild(this.b, this.t)

    if (prop.cbvar) this.c.cbvar = prop[prop.cbvar]

    this.setPos = function (p) {
      this.c.x = p.x
      this.c.y = p.y
    }

    this.c.setColor = function (color) {
      this.children[0].graphics.c().beginFill(color).drawRoundRect(0, 0, prop.width, prop.height, 10)
    }

    this.c.resetColor = function () {
      this.children[0].graphics.c().beginFill(prop.color).drawRoundRect(0, 0, prop.width, prop.height, 10)
    }

    this.c.resetGroupColor = function () {
      for (var i = 0; i < this.group.length; i++) {
        this.group[i].c.resetColor()
      }
    }

    this.c.setText = function (_text) {
      this.children[1].text = _text
    }

    this.c.resetColor()

    // this.t.textAlign = "left";
    // this.t.textBaseline = "middle";
    this.t.x = 10 // prop.width / 2;
    this.t.y = 10 // prop.height / 2;
    this.t.lineWidth = prop.width - 20

    if (prop.center) {
      this.c.regX = prop.width / 2
      this.c.regY = prop.height / 2
    }
    this.c.text = this.t.text

    this.setPos(prop.place)
    this.c.on('click', prop.cb)
    this.c.name = prop.name

    container.addChild(this.c)
  }

  function circleMenu (_param) {
    let p = {
      mText: '24px Arial',
      mTextColor: 'black',
      mX: MTLG.getOptions().width / 2,
      mY: MTLG.getOptions().height / 2,
      mColor: ['rgba(119, 249, 5, 0.6)', 'rgba(249, 71, 5, 0.6)', 'rgba(20, 83, 246, 0.6)', 'rgba(194, 20, 246, 0.6)'],
      mStrokeColor: 'rgba(0, 0, 0, 1)',
      mStroke: 1,
      iconData: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB3aWR0aD0iNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTAgMGg0OHY0OEgweiIgZmlsbD0ibm9uZSIvPjxwYXRoIGQ9Ik0yNCA0QzEyLjk1IDQgNCAxMi45NSA0IDI0czguOTUgMjAgMjAgMjAgMjAtOC45NSAyMC0yMFMzNS4wNSA0IDI0IDR6bS00IDI5VjE1bDEyIDktMTIgOXoiLz48L3N2Zz4='
    }
    for (let prop in _param) {
      p[prop] = _param[prop]
    }

    if (_param && _param.mKeys) {
      // circle menu
      let circleMenuContainer = new createjs.Container()
      circleMenuContainer.x = p.mX
      circleMenuContainer.y = p.mY

      let innerCircle = new createjs.Container()
      innerCircle.name = 'innerCircle'
      let deltaAngle = 360 / p.mKeys.length
      let min = 200 // red playbbutton
      let max = MTLG.getOptions().height / 2 - 50 - 100
      let _colorArr = p.mColor || ['rgba(119, 249, 5, 0.6)', 'rgba(249, 71, 5, 0.6)', 'rgba(20, 83, 246, 0.6)', 'rgba(194, 20, 246, 0.6)']

      let sectionC = new createjs.Container()
      for (var j = 0; j < p.mKeys.length; j++) {
        let _text = MTLG.utils.gfx.getText(MTLG.l(p.mKeys[j]), p.mText, p.mTextColor)
        _text.regX = _text.getBounds().width / 2
        _text.regY = _text.getBounds().height / 2
        _text.x = Math.cos(deltaAngle * Math.PI / 180 / 2) * (min + (max - min) / 2)
        _text.y = Math.sin(deltaAngle * Math.PI / 180 / 2) * (min + (max - min) / 2)
        calcRotation(_text, { x: 0, y: 0 })

        let startAngle = 0
        let endAngle = deltaAngle * Math.PI / 180

        let _cut = MTLG.utils.gfx.getShape()
        _cut.name = 'sectionShape'
        _cut.graphics.clear()
        if (p.mBG && p.mBG[j]) {
          // _cut.graphics.beginBitmapFill(MTLG.assets.getAsset(p.mBG[j]))
          let m = new createjs.Matrix2D()
          let img = MTLG.assets.getAsset(p.mBG[j].src)
          let scale = p.mBG[j].scale
          if (scale) {
            m.scale(scale, scale)
          } else {
            m.scale(0.3, 0.3)
          }
          m.tx = -p.mBG[j].tx || -img.width / 2 * 0.3 + min
          m.ty = -p.mBG[j].ty || -img.height / 2 * 0.3 + min
          if (p.mBG[j].tx === 0) {
            m.tx = 0
          }
          if (p.mBG[j].ty === 0) {
            m.ty = 0
          }
          _cut.graphics.beginBitmapFill(img, null, m)
        } else {
          _cut.graphics.f(_colorArr[j])
        }
        _cut.graphics.ss(p.mStroke).s(p.mStrokeColor).a(0, 0, min, startAngle, endAngle).a(0, 0, max, endAngle, startAngle, true)
        _cut.shadow = new createjs.Shadow(p.shadow, 0, 0, 10)
        _cut.game = { min, max, startAngle, endAngle, c: _colorArr[j] }

        let _cutC = new createjs.Container()
        _cutC.addChild(_cut, _text)
        _cutC.rotation = deltaAngle * j
        sectionC.addChild(_cutC)
      }
      innerCircle.addChild(sectionC)

      innerCircle.highlightSection = function (degree) {
        let fac
        // which section
        let _secNbr = Math.floor(degree / deltaAngle)
        this.secNbr = _secNbr // save active section number
        let sections = this.children[0].children
        for (var i = 0; i < sections.length; i++) {
          let sShape = sections[i].getChildByName('sectionShape')
          let _set = sShape.game
          // highlight
          _secNbr === i ? fac = 1.1 : fac = 1
          let g = sShape.graphics
          g.clear()
          if (p.mBG && p.mBG[i]) {
            let m = new createjs.Matrix2D()
            let img = MTLG.assets.getAsset(p.mBG[i].src)
            let scale = p.mBG[i].scale
            if (scale) {
              m.scale(scale, scale)
            } else {
              m.scale(0.3, 0.3)
            }
            m.tx = -p.mBG[i].tx || -img.width / 2 * 0.3 + min
            m.ty = -p.mBG[i].ty || -img.height / 2 * 0.3 + min
            if (p.mBG[i].tx === 0) {
              m.tx = 0
            }
            if (p.mBG[i].ty === 0) {
              m.ty = 0
            }
            g.beginBitmapFill(img, null, m)
          } else {
            g.f(_set.c)
          }
          g.ss(p.mStroke).s(p.mStrokeColor)
          g.a(0, 0, _set.min, _set.startAngle, _set.endAngle).a(0, 0, _set.max * fac, _set.endAngle, _set.startAngle, true).lt(_set.min, 0)
        }
      }
      innerCircle.jumpToMiddle = function (degree) {
        return deltaAngle / 2 + this.secNbr * deltaAngle
      }
      innerCircle.getActiveSec = function () {
        return this.secNbr
      }

      let playButton = new createjs.Container()
      playButton.name = 'playButton'
      let playButtonBG = MTLG.utils.gfx.getShape()
      playButtonBG.name = 'playButtonBG'
      playButton.addChild(playButtonBG)
      playButton.show = function (msg) {
        let pBG = this.getChildByName('playButtonBG')
        pBG.graphics.clear()
        pBG.graphics.ss(9).s('rgba(48, 48, 48, 1)')
        let j = this.parent.secNbr
        if (p.mPlBG && p.mPlBG[j]) {
          // _cut.graphics.beginBitmapFill(MTLG.assets.getAsset(p.mBG[j]))
          let m = new createjs.Matrix2D()
          let img = MTLG.assets.getAsset(p.mPlBG[j].src)
          m.scale(p.mPlBG[j].scale, p.mPlBG[j].scale)
          m.tx = p.mPlBG[j].tx
          m.ty = p.mPlBG[j].ty
          pBG.graphics.beginBitmapFill(img, null, m)
        }
        pBG.graphics.drawCircle(0, 0, 100)
        pBG.shadow = new createjs.Shadow(p.shadow, 0, 0, 100)

        let icon = this.getChildByName('playIcon')
        if (!icon) {
          // p.iconData = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB3aWR0aD0iNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTAgMGg0OHY0OEgweiIgZmlsbD0ibm9uZSIvPjxwYXRoIGQ9Ik0yMCAzM2wxMi05LTEyLTl2MTh6bTQtMjlDMTIuOTUgNCA0IDEyLjk1IDQgMjRzOC45NSAyMCAyMCAyMCAyMC04Ljk1IDIwLTIwUzM1LjA1IDQgMjQgNHptMCAzNmMtOC44MiAwLTE2LTcuMTgtMTYtMTZTMTUuMTggOCAyNCA4czE2IDcuMTggMTYgMTYtNy4xOCAxNi0xNiAxNnoiLz48L3N2Zz4='
          let img = document.createElement('IMG')
          let imgCon = new createjs.Container()
          imgCon.name = 'iconContainer'
          img.setAttribute('src', p.iconData)
          img.addEventListener('load', function () {
            icon = new createjs.Bitmap(this)
            icon.name = 'playIcon'
            icon.regX = icon.image.width / 2
            icon.regY = icon.image.height / 2
            icon.scaleX = icon.scaleY = 76 / icon.image.width
            let circle = MTLG.utils.gfx.getShape()
            circle.graphics.beginFill(p.mTextColor).drawCircle(0, 0, icon.image.width / 2 * icon.scaleX * 0.8)
            imgCon.addChild(circle)
            imgCon.addChild(icon)
            playButton.addChild(imgCon)
            createjs.Tween.get(imgCon)
              .to({ rotation: 30, scaleX: 1.4 }, 200, createjs.Ease.linear)
              .to({ rotation: -30, scaleX: 1 }, 400, createjs.Ease.linear)
              .to({ rotation: 0 }, 200, createjs.Ease.linear)
          })
        }

        // let text = this.getChildByName('text')
        // if (!text) {
        //   let text = MTLG.utils.gfx.getText(MTLG.l(msg), '36px Arial', 'rgb(230, 228, 205)')
        //   text.name = 'text'
        //   text.regX = text.getBounds().width / 2
        //   text.regY = text.getBounds().height / 2
        //   // this.addChild(text)
        //   createjs.Tween.get(text, { loop: true })
        //     .to({ rotation: 360 }, 6000, createjs.Ease.linear)
        // } else {
        //   text.text = MTLG.l(msg)
        // }
      }
      playButton.on('mousedown', function (e) {
        let target = e.target.parent
        if (target.name === 'iconContainer') {
          target = target.parent
        }
        createjs.Tween.get(target)
          .to({ scaleX: 0.9, scaleY: 0.9 }, 100)
      })
      playButton.on('pressup', function (e) {
        let target = e.target.parent
        if (target.name === 'iconContainer') {
          target = target.parent
        }
        createjs.Tween.get(target)
          .to({ scaleX: 1, scaleY: 1 }, 100)
          .to({ rotation: 360 }, 1000)
          .call(function () {
            MTLG.loadSettings({
              default: {
                game: p.mKeys[this.parent.secNbr]
              }
            })
            MTLG.clearBackground()
            // target._stageTicker = createjs.Ticker.addEventListener('tick', function () {
            //   MTLG.getBackgroundStage().update()
            // })
            MTLG.setBackgroundContainer(showLoading())
            MTLG.lc.goToLogin()
          })
      })
      innerCircle.addChild(playButton)

      let outerCircle = new createjs.Container()
      // let outerCircleBG = new createjs.Shape()
      // if (p.oBG) {
      //   outerCircleBG.graphics.beginBitmapFill(MTLG.assets.getAsset(p.oBG))
      // } else {
      //   outerCircleBG.graphics.beginFill('rgba(61, 163, 105, 0.76)')
      // }
      // outerCircleBG.graphics.a(0, 0, MTLG.getOptions().height / 4, 0, 2 * Math.PI).a(0, 0, MTLG.getOptions().height / 2 - 50, 0, 2 * Math.PI, true)
      // outerCircle.addChild(outerCircleBG)
      var chooser
      if (p.cBG) {
        chooser = MTLG.bmp(p.cBG)
        chooser.hitArea = MTLG.bmp(p.cBG)
        chooser.scaleX = chooser.scaleY = 0.16
        chooser.setBounds(0, 0, chooser.getBounds().width * 0.16, chooser.getBounds().height * 0.16)
        chooser.y = chooser.getBounds().width / 2
        chooser.x = max - chooser.getBounds().height / 3
        chooser.rotation = -90
      } else {
        chooser = MTLG.utils.gfx.getShape()
        chooser.graphics.beginLinearGradientFill(['rgb(116, 177, 212)', 'rgba(35, 23, 130, 1)'], [0, 1], 0, 0, 150, 150).mt(0, 0).lt(130, -40).lt(130, 40).lt(0.0)
        chooser.setBounds(0, 0, 80, 130)
        chooser.y = 0
        chooser.x = max - 50
        chooser.shadow = new createjs.Shadow(p.shadow, 0, 0, 10)
      }
      chooser.name = 'chooser'

      var pressmove = function (e) {
        let coords = e.target.localToLocal(e.localX, e.localY, MTLG.getStageContainer())
        let coordsOuter = e.target.parent.localToLocal(e.target.parent.x, e.target.parent.y, MTLG.getStageContainer())
        let ankathete = coordsOuter.y - coords.y
        let gegenkathete = coordsOuter.x - coords.x
        let degree
        let delta = Math.sqrt(Math.pow(ankathete, 2) + Math.pow(gegenkathete, 2))

        if (ankathete > 0) {
          degree = (-((Math.atan(gegenkathete / ankathete) * 180 / Math.PI)) % 360) + 360
        } else {
          degree = -((Math.atan(gegenkathete / ankathete) * 180 / Math.PI) - 180) % 360
        }

        if (delta > max - chooser.getBounds().height / 3 && delta <= max + 2 * chooser.getBounds().height / 3) {
          e.target.parent.rotation = degree - 90
        } else {
          e.target.dispatchEvent('pressup')
          // this.removeAllEventListeners()
          // this.addAllEventlisteners()
        }
        this.parent.parent.getChildByName('innerCircle').highlightSection(degree - 90)
      }
      chooser.addAllEventlisteners = function () {
        this.on('mousedown', function (e) {
          e.target.fb.hand.deleteFB()
        })
        this.on('pressmove', pressmove)
        this.on('pressup', function (e) {
          this.parent.rotation = this.parent.parent.getChildByName('innerCircle').jumpToMiddle(this.parent.rotation)
          // show start game on play button
          this.parent.parent.getChildByName('innerCircle').getChildByName('playButton').show('Startbutton')
        })
      }
      chooser.addAllEventlisteners()
      outerCircle.addChild(chooser)
      circleMenuContainer.addChild(innerCircle, outerCircle)
      circleMenuContainer.addDragFeedback = function () {
        let chooser = this.getChildAt(1).getChildAt(0)
        let coords = chooser.localToLocal(chooser.getBounds().height / 2, 20, MTLG.getStageContainer())
        var movetotarget = MTLG.utils.fbm.createContent('movetotarget')
        movetotarget.pointCounter = 3
        movetotarget.pointStartX = coords.x / MTLG.getOptions().width // (chooser.parent.parent.x + 450) / MTLG.getOptions().width
        movetotarget.pointStartY = coords.y / MTLG.getOptions().height// chooser.parent.parent.y / MTLG.getOptions().height
        movetotarget.pointEndX = coords.x / MTLG.getOptions().width// (chooser.parent.parent.x + 450) / MTLG.getOptions().width
        movetotarget.pointEndY = (coords.y + 100) / MTLG.getOptions().height// (chooser.parent.parent.y + 100) / MTLG.getOptions().height
        movetotarget.pointRotation = 0
        movetotarget.pointSpeed = 60
        movetotarget.pointSize = 90

        chooser.fb = MTLG.utils.fbm.giveFeedback(chooser, 'movetotarget', 'overallTiming', 'neutralStyle')
      }
      return circleMenuContainer
    } else {
      console.warn('No keys defined in Circle Menu')
      return new createjs.Container()
    }
  }

  function showLoading () {
    let con = new createjs.Container()
    con.name = 'con'

    // Create a shape
    var bar = MTLG.utils.gfx.getShape()
      .set({ x: MTLG.getOptions().width / 2 - 302, y: MTLG.getOptions().height / 2 - 16 }) // Move away from the top left.
    con.addChild(bar)

    // Draw the outline
    bar.graphics.setStrokeStyle(2)
      .beginStroke('white')
      .drawRect(-1, -1, 604, 32)
      .endStroke()

    // Draw the fill. Only set the style here
    var fill = MTLG.utils.gfx.getShape().set({ x: MTLG.getOptions().width / 2 - 302, y: MTLG.getOptions().height / 2 - 16, scaleX: 0.8 })
    fill.graphics.beginFill('orange').drawRect(0, 0, 600, 30)
    fill.name = 'bar'
    con.addChild(fill)
    // createjs.Tween.get(fill, { loop: true })
    //   .to({ scaleX: 1 }, 200, createjs.Ease.linear)
    return con
  }

  return {
    circleMenu,
    buttonsSelectionGroup,
    buttonNbrGroup,
    Button
  }
})()
