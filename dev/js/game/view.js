/**
 * @Author: thiemo
 * @Date:   2017-10-17T11:47:22+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-09-03T21:07:15+02:00
 */

function View (stage) { // eslint-disable-line no-unused-vars
  let _hitAreas = [] // Array of all hitAreas
  let _timerAnimations = []
  let _animationController

  _timerAnimations.getfreeIndex = function () {
    for (var i = 0, l = this.length; i < l; i++) {
      if (typeof this[i] === 'undefined') return i
    }
    return 'pop'
  }

  function AnimationController () {
    var timerAnimations = []

    this.resetAnimations = function () {
      if (timerAnimations.length) {
        let t = timerAnimations
        for (var i = 0, l = t.length; i < l; i++) {
          if (t[i] && (createjs.Ticker.getTime() > t[i].startTime + t[i].duration)) {
            t[i].word.resetAnimation(t[i].type)
            timerAnimations.splice(i, 1)
            break
          }
        }
      }
    }

    this.subscribe = function (fb) {
      timerAnimations.push(fb)
    }
  }

  /**
   * this - description
   *
   * @param  {type} _gStatus description
   * @return {type}          description
   */
  this.init = function (_gStatus) {
    // setTimeout(() => lcdmC.startSession(_gStatus), 1000)
    // MTLG.getStage().addChild(this.showLoading())
    MTLG.getStageContainer().visible = false

    this._addAreas((MTLG.getSettings().design) ? MTLG.getSettings().design.pattern.background : 'rgba(126, 126, 124, 0.5)', 350, 200, _gStatus.players, _gStatus.areas)
    this._addRegEx(_gStatus.areas, _gStatus.players)
    this._addPlayerPoints(_gStatus.areas, 'black')

    _animationController = new AnimationController()
    createjs.Ticker.addEventListener('tick', _animationController.resetAnimations)

    this.addWords(_gStatus.wordCards)
    setTimeout(function () {
      MTLG.getStageContainer().visible = true
    }, _gStatus.delayWords * 1000)
  }

  this.addWords = function (words) {
    this._addWordCards(words)
    this._spreadWords(words)
    MTLG.clearBackground()
    MTLG.setBackgroundImageFill(MTLG.getSettings().default.bgImage)
  }

  /**
   * this - description
   *
   * @param  {type} words description
   * @param  {type} regEX description
   * @return {type}       description
   */
  this.update = function (_gStatus, _bestPlayers) {
    for (var i = 0, j = _hitAreas.length; i < j; i++) {
      // update Area -> PlayerPoints
      let pointsContainer = _hitAreas[i].getChildByName('pointContainer')

      pointsContainer.setText(_gStatus.areas[i].players[0].score)
      pointsContainer.update()

      for (var k = 0, m = _bestPlayers.length; k < m; k++) {
        if (_gStatus.areas[i].players[0].id === _bestPlayers[k].id) {
          pointsContainer.update('rgba(212, 175, 55, 1.0)')
        }
      }
    }
  }

  /**
   * this - add a new word to the stage
   *
   * @param  {type} words description
   * @return {type}       description
   */
  this.newWords = function (words, wIDs) {
    this._addWordCards(words, {
      newID: wIDs
    })
    this._randomPlaceWords(words)
  }

  this.deleteWords = function (wordIds) {
    let wId, child
    for (wId of wordIds) {
      for (child of stage.children) {
        if (child.wId === wId) {
          child.disappear()
        }
      }
    }
  }

  this.endGame = function (_gStatus, cb) {
    this._addEndStatus('rgba(126, 126, 124, 0.5)', 350, 200, _gStatus.players, _gStatus.areas)
    this._addId(_gStatus.areas, _gStatus.players, 'black')

    console.log('-------------')
    console.log('game ends')
    console.log('-------------')

    if (cb) setTimeout(cb, 10000)
  }

  // -----------------------------------------------------------------------
  // ---------------- animate functions --------------------------------------
  // -----------------------------------------------------------------------

  this.levelTransition = function (_text, cb) {
    // TODO should be a framework function
    // let scaleX = window.innerWidth / MTLG.getOptions().width;
    // let scaleY = window.innerHeight / MTLG.getOptions().height;
    // let scaleFactor = 1; //Math.min(scaleX, scaleY);

    // half tranparent overlay
    let ol = new createjs.Container()
    let gray = new createjs.Shape()
    gray.graphics.beginFill('rgba(68, 68, 68, 0.8)').drawRoundRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height, 10)
    // ol.x = ol.y = 0;

    // img https://pixabay.com/de/haken-h%C3%A4kchen-check-abgeschlossen-1727484/
    /* var _img = MTLG.assets.getBitmap('img/hook-1727484_1280.png');
    _img.scaleX = _img.scaleY = 0.2;
    _img.x = MTLG.getOptions().width / 2 - _img.getBounds().width * _img.scaleX / 2;
    _img.y = MTLG.getOptions().height / 2 - _img.getBounds().height * _img.scaleY / 2; */

    // rotating Text
    var text = new createjs.Text(_text, 'bold 48px Arial', '#000000')
    text.x = text.y = 0
    text.regX = text.getBounds().width / 2
    text.regY = text.getBounds().height / 2

    var c = new createjs.Container()
    c.x = MTLG.getOptions().width / 2
    c.y = MTLG.getOptions().height / 2

    c.addChild(text)
    createjs.Tween.get(c, {
      loop: true
    })
      .to({
        rotation: 360
      }, 6000, createjs.Ease.linear)

    ol.addChild(gray, c)
    stage.addChild(ol)

    var firstEvent = true

    // touch ends overlay
    setTimeout(function () {
      ol.on('click', function (evt) {
        if (firstEvent) {
          firstEvent = false
          evt.remove()
          cb(evt)
        }
      })
    }, 2000)

    // TODO Play Sound
  }

  // -----------------------------------------------------------------------
  // ---------------- feedback functions -----------------------------------
  // -----------------------------------------------------------------------

  this.feedback = function (areaView, wordView, status, fbOptions) {
    let i, j
    if (fbOptions == null) console.warn('Empty feedback options in function feedbackWord.')
    else {
      let _fbOptions = fbOptions[0] // right player?
      // let color = _fbOptions.params.color

      // TODO better solution
      let _types = ['color', 'aura', 'shake', 'sound', 'colorArea', 'turnAround']
      let _type = _fbOptions.type.toString(2)
      while (_type.length < 6) {
        _type = '0' + _type
      }

      let type = []

      for (i = _type.length - 1, j = 0; i > -1; i--) {
        if (_type[i] === '1') {
          type.push(_types[j])
        }
        j++
      }

      let _ptypes = ['fly', 'disappear']
      let _ptype = _fbOptions.posttype.toString(2)
      let posttype = []

      for (i = _ptype.length - 1, j = 0; i > -1; i--) {
        if (_ptype[i] === '1') {
          posttype.push(_ptypes[j])
        }
        j++
      }

      _fbOptions.type = type
      _fbOptions.posttype = posttype

      this.feedbackArea(areaView, status, _fbOptions)
      this.feedbackWord(wordView, status, _fbOptions)
    }
  }

  /**
   * this - description
   *
   * @param  {type} area      description
   * @param  {type} status    description
   * @param  {type} fbOptions description
   * @return {type}           description
   */
  this.feedbackArea = function (area, status, _fbOptions) {
    // delay
    if ('delay' in _fbOptions) setTimeout(handleFeedback, _fbOptions.delay * 1000)

    // handle Feedback
    function handleFeedback () {
      let unhandledFb = ['aura', 'shake', 'sound', 'turnAround']
      for (var i = 0; i < _fbOptions.type.length; i++) {
        switch (_fbOptions.type[i]) {
          case 'color':
            area.getChildByName('regExContainer').changeColor(_fbOptions.params.color)
            break
          case 'colorArea':
            console.warn('Not implemented yet ' + _fbOptions.type[i] + ' ' + 'feedbackArea')
            break
          default:
            if (unhandledFb.indexOf(_fbOptions.type[i]) === -1) console.warn('Unhandled feedback option in feedbackArea ' + _fbOptions.type[i])
        }
      }
    }
  }

  /**
   * this - description
   *
   * @param  {type} area      description
   * @param  {type} status    description
   * @param  {type} fbOptions description
   * @return {type}           description
   */
  this.feedbackWord = function (word, status, _fbOptions) {
    let type = _fbOptions.type
    let posttype = _fbOptions.posttype
    let color = _fbOptions.params.color

    // delay
    if ('delay' in _fbOptions) setTimeout(animateFbWord, _fbOptions.delay)

    // feedback
    function animateFbWord () {
      let startTime = createjs.Ticker.getTime()

      // iterate feedback types
      for (var i = 0, j = type.length; i < j; i++) {
        // console.log(type[i]);
        animate(type[i])
      }

      _animationController.subscribe({
        word,
        type,
        startTime,
        duration: _fbOptions.duration
      })

      // start post after delay
      setTimeout(animatePosts, _fbOptions.duration * 1.1)
    }

    function animate (type) {
      switch (type) {
        case 'turnAround':
          word.turnAround()
          break
        case 'shake':
          word.shake()
          break
        case 'color':
          word.changeColor(color)
          break
        case 'aura':
          word.addAura(color)
          break
        case 'sound':
          word.sound(status)
          break
        default:
          console.warn('Unhandled feedback option in feedbackWord ' + type)
      }
    }

    function animatePosts () {
      for (var i = 0, k = posttype.length; i < k; i++) {
        animatePost(posttype[i])
      }
    }

    function animatePost (type) {
      switch (type) {
        case 'fly':
          word.fly()
          break
        case 'disappear':
          word.disappear()
          break
        default:
          console.warn('Unhandled post feedback option in feedbackWord ' + type)
      }
      if (!status) word.addListener()
    }
  }

  // -----------------------------------------------------------------------
  // ---------------- internal functions -----------------------------------
  // -----------------------------------------------------------------------

  // TODO abstract an general information object added at the area
  this._addPlayerPoints = function (areas, textcolor) {
    function PointTextContainer (regExpText, area) {
      let regExT = new createjs.Text(regExpText, '28px Arial', textcolor)
      let bg = new createjs.Shape()

      regExT.textAlign = 'center'
      regExT.textBaseline = 'middle'

      bg.setSize = function (bounds, color) {
        let _color = color || 'rgb(91, 91, 91)'
        this.graphics.beginFill(_color).drawCircle(0, 0, bounds)
      }

      this.c = new createjs.Container()
      this.c.update = function (color) {
        let bounds = regExT.getBounds().width
        if (bounds > 16) bounds *= 0.7
        bg.setSize(bounds, color)
      }
      this.c.setText = function (text) {
        regExT.text = text
      }
      this.c.setBounds(0, 0, regExT.getBounds().width, regExT.getBounds().height)
      this.c.x = area.getBounds().width
      this.c.y = regExT.getBounds().height
      this.c.addChild(bg, regExT)
      this.c.name = 'pointContainer'
      this.c.setText(regExpText)
      this.c.update()

      return this.c
    }

    for (var i = 0, l = areas.length; i < l; i++) {
      _hitAreas[i].addChild(new PointTextContainer(areas[i].players[0].score, _hitAreas[i]))
    }
  }

  /**
   * this - description
   *
   * @param  {type} _arrWordCards description
   * @param  {type} textColor     description
   * @param  {type} wordBgColor   description
   * @return {type}               description
   */
  this._addWordCards = function (_arrWordCards, o) {
    let _word, pic
    let __o = o || {}
    let _o = {
      textColor: __o.textColor,
      wordBgColor: __o.wordBgColor,
      newID: __o.newID || 0
    }

    var wordsDesign = {
      font: 'Arial',
      fontType: 'bold',
      fontColor: _o.textColor || 'rgb(255, 255, 255)',
      background: __o.wordBgColor || 'rgb(0, 0, 255)'
    }
    if (MTLG.getSettings().design) {
      for (var prop in MTLG.getSettings().design.words) {
        if (MTLG.getSettings().design.words[prop]) wordsDesign[prop] = MTLG.getSettings().design.words[prop]
      }
    }

    let pics = MTLG.getSettings().default.wordView === 'pictures'


    function _wordC (text, color, textcolor, font, fontType) {
      this.label = new createjs.Text(text, fontType + ' 24px ' + font, textcolor)
      this.label.textAlign = 'center'
      this.label.textBaseline = 'middle'
      this.bounds = this.label.getBounds()
      this.rect = new createjs.Shape()
      this.rect.graphics.beginStroke('rgb(0, 0, 0)').beginFill(color).drawRoundRect(0, 0, this.bounds.width + 50, this.bounds.height + 50, 10)
      this.rect.name = text
	  this.rect.setBounds(0, 0, this.bounds.width + 50, this.bounds.height + 50)
      this.label.x = this.rect.getBounds().width / 2
      this.label.y = this.rect.getBounds().height / 2
      //Eye-Tracking Words               
      MTLG.eyetracking.registerListenerForObject(this.rect, () => console.log("looked at word: "+text, arguments), 500, 500);
    }

    for (var i = 0, l = _arrWordCards.length; i < l; i++) {
      _arrWordCards[i].shape = new createjs.Container()
      _arrWordCards[i].shape.name = 'wordCard'
      _arrWordCards[i].shape.color = _o.wordBgColor

      _word = new _wordC(_arrWordCards[i].word, wordsDesign.background, wordsDesign.fontColor, wordsDesign.font, wordsDesign.fontType)
      _arrWordCards[i].shape.regX = _word.rect.getBounds().width / 2
      _arrWordCards[i].shape.regY = _word.rect.getBounds().height / 2
      _arrWordCards[i].shape.setBounds(0, 0, _word.rect.getBounds().width, _word.rect.getBounds().height)
      _arrWordCards[i].shape.changeColor = function (color) {
        this.word.rect.graphics.beginFill(color).drawRoundRect(0, 0, this.getBounds().width, this.getBounds().height, 10)
      }
      _arrWordCards[i].shape.resetColor = function () {
        this.word.rect.graphics.beginFill(wordsDesign.background).drawRoundRect(0, 0, this.getBounds().width, this.getBounds().height, 10)
      }
      _arrWordCards[i].shape.addChild(_word.rect, _word.label)

      // replace word with a picture if available
      if (pics) {
        let path = MTLG.getSettings().default.bgPath || ''
        pic = MTLG.bmp('img/games/' + path + _arrWordCards[i].word + '.png')
        if (pic.image) {
          let _scaleFactor = pic.image.width > pic.image.height ? pic.image.width : pic.image.height
          pic.scaleX = pic.scaleY = 150 / _scaleFactor
          _arrWordCards[i].shape.regX = pic.image.width / 2 * pic.scaleX
          _arrWordCards[i].shape.regY = pic.image.height / 2 * pic.scaleY
          _arrWordCards[i].shape.setBounds(0, 0, pic.image.width * pic.scaleX, pic.image.height * pic.scaleY)
          let hit = new createjs.Shape()
          hit.graphics.beginFill('#000').rect(0, 0, pic.image.width * pic.scaleX, pic.image.height * pic.scaleY)
          _arrWordCards[i].shape.hitArea = hit
          _arrWordCards[i].shape.changeColor = function (color) {}
          _arrWordCards[i].shape.resetColor = function (color) {}
          _arrWordCards[i].shape.children[0].visible = false
          _arrWordCards[i].shape.children[1].visible = false
          _arrWordCards[i].shape.addChild(pic)
        }
      }

      _arrWordCards[i].shape.word = _word

      _arrWordCards[i].shape.wId = i + _o.newID
      _arrWordCards[i].shape.on('pressmove', draggable)
      _arrWordCards[i].shape.on('pressup', hitObject)
      _arrWordCards[i].shape.on('mousedown', startListening)
      _arrWordCards[i].shape.addListener = function () {
        this.on('pressmove', draggable)
        this.on('pressup', hitObject)
        this.on('mousedown', startListening)
      }

      _arrWordCards[i].shape.addAura = function (color) {
        let aura = new createjs.Shape()
        aura.graphics.beginFill(color).drawRoundRect(0, 0, this.getBounds().width + 30, this.getBounds().height + 30, 10)
        aura.x = aura.y = -15
        aura.alpha = 0.5
        this.addChild(aura)
        this.setChildIndex(aura, 0)
      }
      _arrWordCards[i].shape.resetAura = function () {
        this.removeChildAt(0)
      }
      _arrWordCards[i].shape.turnAround = function () {
        let _rotation = this.rotation
        this.tween = createjs.Tween.get(this, {
          loop: true
        })
          .to({
            rotation: _rotation + 360
          }, 1000)
        return this.tween
      }
      _arrWordCards[i].shape.shake = function () {
        let _rotation = this.rotation
        this.tween = createjs.Tween.get(this, {
          loop: true
        })
          .to({
            rotation: _rotation - 40
          }, 100)
          .to({
            rotation: _rotation + 40
          }, 100)
        return this.tween
      }
      _arrWordCards[i].shape.resetTween = function () {
        createjs.Tween.removeTweens(this)
      }
      _arrWordCards[i].shape.sound = function (status) {
        if (status) {
          // good sound
          MTLG.assets.playSound('feedback/word_pos')
        } else {
          // bad sound
          MTLG.assets.playSound('feedback/word_neg')
        }
      }
      _arrWordCards[i].shape.disappear = function () {
        stage.removeChild(this)
      }
      _arrWordCards[i].shape.fly = function () {
        let x = Math.random() * (MTLG.getOptions().width - 200) + 100
        let y = Math.random() * (MTLG.getOptions().height - 500) + 250
        this.tween = createjs.Tween.get(this)
          .to({
            x: x,
            y: y
          }, 300)
      }
      _arrWordCards[i].shape.resetAnimation = function (type) {
        for (var i = 0; i < type.length; i++) {
          switch (type[i]) {
            case 'color':
              this.resetColor()
              break
            case 'aura':
              this.resetAura()
              break
            case 'shake':
              this.resetTween()
              break
            default:
          }
        }
      }
      stage.addChild(_arrWordCards[i].shape)
    }
  }

  this._addId = function (areas, players, textcolor) {
    function RegIdTextContainer (area, areaView) {
      let regExT, bReg, bg

      bReg = 'ID: ' + area.players[0].id

      bg = new createjs.Shape()
      bg.name = 'regExBG'
      bg.setBounds(0, 0, areaView.getBounds().width, areaView.getBounds().height / 4)

      regExT = new createjs.Text(bReg, '48px Arial', textcolor)
      regExT.setTextsize = function () {
        let size = 46
        while (bg.getBounds().width < this.getBounds().width || size < 4) {
          size -= 2
          this.font = size + 'px Arial'
        }
      }
      regExT.name = 'regExText'
      regExT.x = 2
      regExT.y = -3
      regExT.setTextsize()

      this.c = new createjs.Container()
      this.c.setBounds(0, 0, regExT.getBounds().width, regExT.getBounds().height)
      this.c.y = -regExT.getBounds().height
      this.c.name = 'regExContainer'
      this.c.addChild(bg, regExT)
      this.c.changeColor = function (color) {
        this.getChildByName('regExBG').graphics.beginFill(color).drawRoundRect(0, 0, areaView.getBounds().width, areaView.getBounds().height / 4, 10)
      }
      this.c.changeToBasicColor = function () {
        this.getChildByName('regExBG').graphics.beginFill('rgb(87, 102, 238)').drawRoundRect(0, 0, areaView.getBounds().width, areaView.getBounds().height / 4, 10)
      }
      this.c.changeToBasicColor()

      return this.c
    }

    for (var i = 0, l = areas.length; i < l; i++) {
      let r = new RegIdTextContainer(areas[i], _hitAreas[i])
      _hitAreas[i].addChild(r)
      // _hitAreas[i].setChildIndex(r, 0);
    }
  }

  /**
   * this - description
   *
   * @param  {type} color  description
   * @param  {type} width  description
   * @param  {type} height description
   * @return {type}        description
   */
  this._addEndStatus = function (color, width, height, players, areas) {
    // console.log(players);
    // console.log(areas);

    let nbrAreas = areas.length

    if (nbrAreas > 4) {
      nbrAreas = 4
      console.log('Till now 4 areas at max in this view.')
    }

    function Area (plId) {
      // TODO function l not working here
      let playerString = MTLG.l('player') + ' ' + (plId + 1) + ' (' + players[plId].user.name + ')'
      let bg, plName, score, feedback

      this.c = new createjs.Container()
      bg = new createjs.Shape()
      bg.graphics.beginFill(color).drawRoundRect(0, 0, width, height, 10)

      plName = new createjs.Text(playerString, 'bold 16px Arial', 'black')
      score = new createjs.Text('Score: ' + players[plId].score, 'bold 16px Arial', 'black')
      feedback = new createjs.Text('Displays statistics of the feedback controller, if available. This screen disappears in 10s.', 'bold 16px Arial', 'black')

      this.c.setBounds(0, 0, width, height)
      this.c.aId = plId
      plName.y = this.c.getBounds().height - plName.getBounds().height - 2
      plName.x = this.c.getBounds().width / 2 - plName.getBounds().width / 2
      score.y = 10
      feedback.y = score.y + score.getBounds().height * 2
      feedback.lineWidth = this.c.getBounds().width

      // TODO Statistics of the feedback controller

      this.c.addChild(bg, plName, score, feedback)
      this.c.name = 'area'
      stage.addChild(this.c)
      return this.c
    }

    for (var i = 0, k = nbrAreas; i < k; i++) {
      let _area = new Area(i)
      let _offsetX = _area.getBounds().width / 2
      let _offsetY = _area.getBounds().height

      if (i === 0 || i === 2) _area.x = MTLG.getOptions().width / 4 - _offsetX
      if (i === 1 || i === 3) _area.x = MTLG.getOptions().width / 4 * 3 - _offsetX

      if (i === 0 || i === 1) {
        _area.y = _offsetY
        _area.rotation = 180
        _area.x = _area.x + _area.getBounds().width
      }
      if (i === 2 || i === 3) _area.y = MTLG.getOptions().height - _offsetY

      _hitAreas.push(_area)
    }
  }

  /**
   * this - description
   *
   * @return {type}  description
   */

  //Eye-Tracking for Targetarea1
  this._addRegEx = function (areas, players) {
    function RegExTextContainer (area, areaView) {
      let regExT, bReg, bg

      bReg = area.regEx[0].exp.toString()
      bReg = MTLG.getSettings().default.regExToString(bReg)
      // console.log(bReg)

      bg = new createjs.Shape()
      bg.name = 'regExBG'
      bg.setBounds(0, 0, areaView.getBounds().width, areaView.getBounds().height / 4)

      var patternDesign = {
        font: 'Arial',
        fontType: 'bold',
        fontColor: 'black',
        fontBackground: 'rgb(87, 102, 238)',
        fontAlign: 'left'
      }
      if (MTLG.getSettings().design) {
        for (var prop in MTLG.getSettings().design.pattern) {
          if (MTLG.getSettings().design.pattern[prop]) patternDesign[prop] = MTLG.getSettings().design.pattern[prop]
        }
      }

      regExT = new createjs.Text(bReg, patternDesign.fontType + ' 48px ' + patternDesign.font, patternDesign.fontColor)
      regExT.setTextsize = function () {
        let size = 46
        while (bg.getBounds().width < this.getBounds().width || size < 4) {
          size -= 2
          this.font = patternDesign.fontType + ' ' + size + 'px ' + patternDesign.font
        }
      }
      regExT.name = 'regExText'

      regExT.textAlign = patternDesign.fontAlign
      regExT.textAlign === 'center' ? regExT.x = bg.getBounds().width / 2 : regExT.x = 2

      regExT.textBaseline = 'middle' // needed with variable font height
      regExT.y = bg.getBounds().height / 2 - 1// changes y-middlepoint according to size
      regExT.setTextsize()

      this.c = new createjs.Container()
      this.c.setBounds(0, 0, regExT.getBounds().width, regExT.getBounds().height)
      this.c.y = -regExT.getBounds().height
      this.c.name = bReg
      this.c.addChild(bg, regExT)
      this.c.changeColor = function (color) {
        this.getChildByName('regExBG').graphics.beginFill(color).drawRoundRect(0, 0, areaView.getBounds().width, areaView.getBounds().height / 4, 10)
      }
      this.c.changeToBasicColor = function () {
        this.getChildByName('regExBG').graphics.beginFill(patternDesign.fontBackground).drawRoundRect(0, 0, areaView.getBounds().width, areaView.getBounds().height / 4, 10)
      }
      this.c.changeToBasicColor()

      //Eye-Tracking TargetArea?
      //MTLG.eyetracking.createArea("Target: "+bReg, 0, 0, regExT.getBounds().width, regExT.getBounds().height, null);
      //MTLG.eyetracking.registerListenerForArea("Target: "+bReg, () => console.log("listener for target:"+bReg, arguments), 1000, 1000);

      //Eye-Tracking on RegEx Expression/Target
      MTLG.eyetracking.registerListenerForObject(this.c, () => console.log("Expression:"+bReg, arguments), 500, 500);

      return this.c
    }

    for (var i = 0, l = areas.length; i < l; i++) {
      let r = new RegExTextContainer(areas[i], _hitAreas[i])
      _hitAreas[i].addChild(r)
      // _hitAreas[i].setChildIndex(r, 0);
    }
  }

  /**
   * this - description
   *
   * @param  {type} color  description
   * @param  {type} width  description
   * @param  {type} height description
   * @return {type}        description
   */
  this._addAreas = function (color, width, height, players, areas) {
    let nbrAreas = areas.length

    if (nbrAreas > 4) {
      nbrAreas = 4
      console.log('Till now 4 areas at max in this view.')
    }

    function Area (plId) {
      // TODO function l not working here
      let playerString = 'Spieler' + ' ' + (plId + 1) + ' (' + players[plId].user.name + ')'
      let bg, plName

      this.c = new createjs.Container()
      bg = new createjs.Shape()
      bg.graphics.beginFill(color).drawRoundRect(0, 0, width, height, 10)

      plName = new createjs.Text(playerString, 'bold 16px Arial', 'black')

      this.c.setBounds(0, 0, width, height)
      this.c.aId = plId
      plName.y = this.c.getBounds().height - plName.getBounds().height - 2
      plName.x = this.c.getBounds().width / 2 - plName.getBounds().width / 2

      // TODO playerName as own object like regEx and points

      this.c.addChild(bg, plName)
      this.c.name = 'area'
      stage.addChild(this.c)
      return this.c
    }

    for (var i = 0, l = nbrAreas; i < l; i++) {
      let _area = new Area(i)
      let _offsetX = _area.getBounds().width / 2
      let _offsetY = _area.getBounds().height

      if (i === 0 || i === 2) _area.x = MTLG.getOptions().width / 4 - _offsetX
      if (i === 1 || i === 3) _area.x = MTLG.getOptions().width / 4 * 3 - _offsetX

      if (i === 0 || i === 1) {
        _area.y = _offsetY
        _area.rotation = 180
        _area.x = _area.x + _area.getBounds().width
      }
      if (i === 2 || i === 3) _area.y = MTLG.getOptions().height - _offsetY

      _hitAreas.push(_area)
    }
  }

  // ------------------------------------------------------------
  // --------------------- controller functions ------------------------
  // ------------------------------------------------------------

  function hitObject (e) {
    if (e.target.wId !== undefined) {
      createjs.Tween.get(e.target)
        .to({ scaleX: 1, scaleY: 1 }, 100)
    } else {
      createjs.Tween.get(e.target.parent)
        .to({ scaleX: 1, scaleY: 1 }, 100)
    }
    let pX = e.stageX
    let pY = e.stageY
    // let bXmin, bYmin, bXmax, bYmax
    let noHit = true

    this.moving = false

    this.endTimestamp = Date.now()
    this.endX = e.stageX
    this.endY = e.stageY

    for (var i = 0, l = _hitAreas.length; i < l; i++) {
      let nX = _hitAreas[i].globalToLocal(pX, pY).x
      let nY = _hitAreas[i].globalToLocal(pX, pY).y
      if (_hitAreas[i].hitTest(nX, nY)) {
        // Presenter.verifyWord(e.currentTarget, _hitAreas[i]);
        this.removeAllEventListeners()
        // this.removeEventListener("pressmove", draggable);
        // this.removeEventListener("pressup", hitObject);
        noHit = false
        Presenter.verifyWord(this, _hitAreas[i], e)
        break
      }
    }
    if (noHit) {
      Presenter.unspecificInteraction(this, e)
    }
    if (e.target.wId !== undefined) {
      Presenter.dragWord(e.target, false)
    } else {
      Presenter.dragWord(e.target.parent, false)
    }
  }

  function startListening (e) {
    if (e.target.wId !== undefined) {
      Presenter.dragWord(e.target, true)
      MTLG.getStageContainer().setChildIndex(e.target, MTLG.getStageContainer().numChildren - 1)
      createjs.Tween.get(e.target)
        .to({ scaleX: 1.1, scaleY: 1.1 }, 100)
    } else {
      createjs.Tween.get(e.target.parent)
        .to({ scaleX: 1.1, scaleY: 1.1 }, 100)
      Presenter.dragWord(e.target.parent, true)
      MTLG.getStageContainer().setChildIndex(e.target.parent, MTLG.getStageContainer().numChildren - 1)
    }

    this.startTimestamp = Date.now()
    this.startY = e.stageY
    this.startX = e.stageX

    // Initialize verbose event logging
    this.moveCoord = []
    this.moveDiff = []

    // for rotation
    let coords = e.target.localToLocal(e.localX, e.localY, MTLG.getStage().children[0]) // no click on container... so we have to look at the parent
    this.startLocalX = coords.x
    this.startLocalY = coords.y
  }

  // ------------------------------------------------------------
  // --------------------- test functions ------------------------
  // ------------------------------------------------------------
  this._randomPlaceWords = function (_wordCards) {
    for (var i = 0, l = _wordCards.length; i < l; i++) {
      let x = Math.random() * (MTLG.getOptions().width - 200) + 100
      let y = Math.random() * (MTLG.getOptions().height - 500) + 250
      _wordCards[i].shape.x = x
      _wordCards[i].shape.y = y
      rotate(_wordCards[i].shape)
    }
  }

  this._spreadWords = function (_wordCards) {
    for (var i = 0, l = _wordCards.length; i < l; i++) {
      let x = Math.random() * (MTLG.getOptions().width - 200) + 100
      let y = Math.random() * (MTLG.getOptions().height - 500) + 250
      let delta = 100
      // console.log(MTLG.getStageContainer().getObjectsUnderPoint(x, y).length)
      for (let j = 0; j < 100; j++) {
        if (
          MTLG.getStageContainer().getObjectsUnderPoint(x + delta, y - delta).length !== 0 ||
          MTLG.getStageContainer().getObjectsUnderPoint(x - delta, y - delta).length !== 0 ||
          MTLG.getStageContainer().getObjectsUnderPoint(x + delta, y + delta).length !== 0 ||
          MTLG.getStageContainer().getObjectsUnderPoint(x - delta, y + delta).length !== 0 ||
          MTLG.getStageContainer().getObjectsUnderPoint(x + delta / 2, y - delta / 2).length !== 0 ||
          MTLG.getStageContainer().getObjectsUnderPoint(x - delta / 2, y - delta / 2).length !== 0 ||
          MTLG.getStageContainer().getObjectsUnderPoint(x + delta / 2, y + delta / 2).length !== 0 ||
          MTLG.getStageContainer().getObjectsUnderPoint(x - delta / 2, y + delta / 2).length !== 0 ||
          MTLG.getStageContainer().getObjectsUnderPoint(x, y).length !== 0
        ) {
          // console.log('loop')
          x = Math.random() * (MTLG.getOptions().width - 200) + 100
          y = Math.random() * (MTLG.getOptions().height - 500) + 250
        } else {
          break
        }
      }
      _wordCards[i].shape.x = x
      _wordCards[i].shape.y = y
      rotate(_wordCards[i].shape)
    }
  }
}
