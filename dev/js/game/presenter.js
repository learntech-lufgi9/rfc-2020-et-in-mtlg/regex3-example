/**
 * @Author: thiemo
 * @Date:   2017-10-17T11:46:47+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-08-29T19:20:59+02:00
 */

var Presenter = (function () { // eslint-disable-line no-unused-vars
  var model
  var view
  var presets
  var deactivated

  // Variables
  // var plusScore = 2; //Score that is added for correct regEx
  // var minusScore = -1; //Score that is added for incorrect regEx

  function init (_model, _view, prop) {
    if (model) {
      model.gameTimer.stop()
    }
    model = _model || {}
    view = _view || {}
    presets = prop || presets

    model.init(presets)
    view.init(model.getGameStatus())

    if (model.getGameStatus().endCondition === 'timer') {
      model.gameTimer.set(model.getGameStatus().maxTime, timerCallback)
      model.gameTimer.start()
    }
    deactivated = false

    // to save regExp to JSON
    Object.defineProperty(RegExp.prototype, 'toJSON', {
      value: RegExp.prototype.toString
    })
  }

  function dragWord (target, state) {
    model.setWordDragged(target.wId, state)
  }

  function verifyWord (wordView, areaView, e) {
    if (deactivated) {
      return
    }; // No verification after game end, dragged cards can no longer trigger additional actions in transition screen.

    let player // default undefined

    // var gameMode = model.getGameStatus().gameMode;
    let status = model.checkWordStatus(wordView.wId, areaView.aId)

    let wId = wordView.wId
    let aId = areaView.aId

    if (status) {
      if (model.addCardToArea(wId, aId)) { // Try to add card to area, only increase score etc afterwards
        model.getCardById(wId).isMatched = true

        switch (model.getGameStatus().scoreType) {
          case 'different':
            model.increaseScoreOfArea(aId, MTLG.getSettings().default.plusScore)
            break
          case 'same':
            model.increaseAllScores(MTLG.getSettings().default.plusScore)
            break
          default:
        }

        if (MTLG.getSettings().default.endCondition === 'timer') {
          let level = 'level' + model.getGameStatus().level
          let pWordPool = model.getGameStatus().wordsArr[level][model.getSettingsVersion()]
          endlessHelper(wId, pWordPool)
        }
      }
    } else {
      switch (model.getGameStatus().scoreType) {
        case 'different':
          model.increaseScoreOfArea(aId, MTLG.getSettings().default.minusScore)
          break
        case 'same':
          model.increaseAllScores(MTLG.getSettings().default.minusScore)
          break
        default:
      }

      model.addWrongCardToArea(wId, aId)
    }

    if (status) { // Add card to new area and remove from old one TODO: is this always correct?
      model.removeCardFromAllAreas(wordView.wId)
      model.addCardToArea(wordView.wId, areaView.aId)
    }

    // Update View --------------
    view.update(model.getGameStatus(), model.getBestPlayers()) // TODO: Render additional symbol for best player

    // Feedback ------------------

    // About movement pattern
    // The coordinate progression is saved in wordView.moveCoord
    // The euclidian distance between coordinate points is saved in wordView.moveDiff

    // identify player if possible
    if (e.nativeEvent.tangible) {
      if (MTLG.getSettings().default.tangibleIDs.indexOf(e.nativeEvent.tangible.tangibleAlias) !== -1) {
        player = MTLG.getSettings().default.tangibleIDs.indexOf(e.nativeEvent.tangible.tangibleAlias)
      }
    }

    // save interaction
    // lcdmC.touchEvent({
    //   word: wordView,
    //   area: areaView,
    //   value: status,
    //   gameState: model.getGameStatus(),
    //   tangible: e.nativeEvent.tangible
    // }, player)

    // Feedback Controller
    let feedbackJSON = fbC.getFeedback({
      word: wordView,
      area: areaView,
      value: status,
      tangible: e.nativeEvent.tangible
    }, player)

    // show feedback
    view.feedback(areaView, wordView, status, feedbackJSON)
    // view.feedbackArea(, status);
    // view.feedbackWord(wordView, , status);

    for (var i = 0; i < feedbackJSON.length; i++) {
      // send tangible feedback
      let tangibleJSON = {
        fb: feedbackJSON[i],
        evt: feedbackJSON[i].tangibleFb.tangibleAlias,
        id: e.pointerID
      }
      console.log(tangibleJSON)
      MTLG.tabulaEvents.sendEvent(tangibleJSON)
    }

    // save interaction
    xAPI.wordEvent({
      word: wordView,
      area: areaView,
      value: status,
      gameState: model.getGameStatus(),
      tangible: e.nativeEvent.tangible
    }, player)

    if (model.getGameStatus().endCondition === 'level') {
      if (model.allCardsMatched()) {
        finishLevel()
      }
    }
  }

  function unspecificInteraction (wordView, e) {
    // let word = Object.assign(wordView)
    // word.endTimestamp = Date.now();

    // lcdmC.undeftouchEvent({
    //   word: word,
    //   gameState: model.getGameStatus(),
    //   tangible: e.nativeEvent.tangible
    // })
  }

  function timerCallback () {
    finishLevel()
  }

  /**
   * Called once the level is done. Draws scores etc on transition screen in view.
   * View calls _levelUp after the review screen was dismissed
   */
  function finishLevel () {
    deactivated = true // No more cards can be verified

    if (model.getGameStatus.endCondition === 'timer') {
      if (!model.gameTimer.timeUp()) {
        console.log('Time remaining in finishLevel. This should not happen.')
        console.log(model.gameTimer)
      }
      // Reset timer to prevent strange behavior where previous timer ends next level
      model.gameTimer.stop()
    }

    // Show Scores (currently in console)
    for (let player of model.getGameStatus().players) {
      console.log('Final scores: ' + player.score)
    }

    console.log(model.getGameStatus())

    // Chose correct Transition Screen
    if ((model.getGameStatus().level) === model.getGameStatus().maxLevel - 1) {
      view.levelTransition(MTLG.l('Game end'), _gameUp)
    } else {
      view.levelTransition(MTLG.l('Level end'), _levelUp)
    }
  }

  function _levelUp () {
    MTLG.lc.levelFinished({
      nextLevel: 2,
      level: 1
    })
  }

  function _gameUp () {
    // TODO Result Screen
    MTLG.lc.goToMenu()
  }

  /**
   * @function newGame
   * Called after the first level to transition to next level or end game if no other levels exist.
   */
  function newGame (_view) {
    view = _view || {}

    // Always increase level, check if this was last level
    model.getGameStatus().level++
    if ((model.getGameStatus().endCondition === 'level' && model.getGameStatus().level > model.getGameStatus().maxLevel - 1) ||
      (model.getGameStatus().endCondition === 'timer' && model.gameTimer.timeUp() && model.getGameStatus().level > model.getGameStatus().maxLevel - 1)) {
      console.log('Game done!')

      model.gameTimer.stop()
      MTLG.lc.goToMenu() // you have to use the menu
    } else {
      if (model.getGameStatus().endCondition === 'timer') {
        // model.gameTimer.set(model.getGameStatus().maxTime, timerCallback)
        model.gameTimer.start()
      }

      model.preset(presets)

      view.init(model.getGameStatus()) // TODO: Reuse old view

      deactivated = false
    }
  }

  function getNewCards () {
    return model.getNewCards()
  }

  function endlessMultiHelper (wId) { // eslint-disable-line no-unused-vars
    let level = 'level' + model.getGameStatus().level
    let pWordPool = MTLG.getSettings().default.constantCoop2Words[level][0] // [model.getSettingsVersion()];
    console.log('endlesshelper')
    endlessHelper(wId, pWordPool)
  }

  /*
   * Creates a matching and two non-matching cards, deleting two non-matching cards in the process.
   * pWordPool is optional, default is constantWords
   */
  function endlessHelper (wId, pWordPool) {
    // var currWord = model.getCardById(wId)
    let level = 'level' + model.getGameStatus().level
    let wordPool = pWordPool || model.getGameStatus().wordsArr[level][0] // [model.getSettingsVersion()];

    let bufferPool = wordPool.matching.filter((x) => {
      return !model.isMatchableExpression(x)
    })
    wordPool.matching = wordPool.matching.filter(model.isMatchableExpression) // TODO: This is a hack. This changes the original arrays
    wordPool.nonMatching = wordPool.nonMatching.concat(bufferPool)

    let pos = Math.floor(wordPool.matching.length * Math.random())
    var label = wordPool.matching[pos]
    model.addWord(label) // Add another matching card

    // Also delete two nonMatching cards to keep the number of cards equal
    // wordPool = MTLG.getSettings().default.constantWords[level][model.getSettingsVersion()];
    let deletedCards = []
    for (var i = 0; i < 2; i++) {
      do {
        let pos = Math.floor(model.getGameStatus().wordCards.length * Math.random())
        let currWord = model.getCardById(pos)
        if (deletedCards.indexOf(pos) > -1 || !currWord.isActive || wordPool.nonMatching.indexOf(currWord.word) < 0 || currWord.isDragged) {
          continue
        } else {
          deletedCards.push(pos)
          currWord.isActive = false // Deactivate card so that it can not be chosen twice
        }
      } while (deletedCards.length <= i)
    }
    for (var j = 0; j < 2; j++) { // And add two nonMatchin cards
      let level = 'level' + model.getGameStatus().level
      // console.log(MTLG.getSettings().default)
      let wordPool = MTLG.getSettings().default.cooperativeWords[level][model.getSettingsVersion()]
      let pos = Math.floor(wordPool.nonMatching.length * Math.random())
      let label = wordPool.nonMatching[pos]
      model.addWord(label)
    }
    view.newWords(model.getNewCards(), model.getGameStatus().wordCards.length - 3)
    view.deleteWords(deletedCards)
  }

  return {
    init: init,
    dragWord: dragWord,
    newGame: newGame,
    verifyWord: verifyWord,
    getNewCards: getNewCards,
    unspecificInteraction: unspecificInteraction
  }
})()
