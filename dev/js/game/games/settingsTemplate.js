
function getTemplate () { // eslint-disable-line no-unused-vars
  let verifyFunction = function (word, pattern) {
    return pattern.test(word)
  }

  let regExToString = function (regEx) {
    return regEx.slice(3, regEx.length - 4)
  }

  let settings = {
    default: {
      gameName: 'Template - RoadShow',
      skipMenu: false,
      verifyFunction: verifyFunction,
      regExToString: regExToString,
      gameMode: 'cooperative',
      useConstant: true, // Use constant regEx/word or generate randomly
      nbrWords: 20,
      timerMaxTime: 1 * 90 * 1000, // Per Game max time in milli-seconds
      delayWords: 1, // delay till the words will appear
      plusScore: 2,
      minusScore: -1,
      alphabet: ['template'],
      /*
      cooperative mode (together)
      */
      cooperativeRegEx: {
        level0: [
          [/^((ab)*)$/i, /^((cd)*)$/i, /^((bc)*)$/i, /^((ad)*)$/i]
        ], // Array of arrays, inner arrays have a regEx for each of the 4 players
        level1: [
          [/^((a|ab)*)$/i, /^((b|ba)*)$/i, /^((c|cd)*)$/i, /^((d|dc)*)$/i]
        ], // Array of arrays, inner arrays have a regEx for each of the 4 players
        level2: [
          [/^((a|b)*ab(a|b)*)$/i, /^((a|d)*ad(a|d)*)$/i, /^((b|c)*bc(b|c)*)$/i, /^((c|d)*cd(c|d)*)$/i]
        ] // Array of arrays, inner arrays have a regEx for each of the 4 players
      },
      cooperativeWords: {
        level0: [{
          matching: ['ab', 'abab', 'ababab', 'cd', 'cdcd', 'cdcdcd', 'bc', 'bcbc', 'bcbcbc', 'ad', 'adad', 'adadad'],
          nonMatching: ['ba', 'aba', 'ababa', 'abaab', 'dc', 'cdc', 'cdcdc', 'cdccd', 'cb', 'cbc', 'cbcbc', 'cbccb', 'da', 'ada', 'adada', 'adaad']
        }],
        level1: [{
          matching: ['a', 'ab', 'aaa', 'aba', 'aaabaa', 'b', 'ba', 'bbb', 'bab', 'bbbabb', 'c', 'cd', 'ccc', 'cdc', 'cccdcc', 'd', 'dc', 'ddd', 'dcd', 'dddcdd'],
          nonMatching: ['ababbb', 'ababb', 'abb', 'bbab', 'aabb', 'babaaa', 'babaa', 'baa', 'aaba', 'bbaa', 'cdcddd', 'cdcdd', 'cdd', 'ddcd', 'ccdd', 'dcdccc', 'dcdcc', 'dcc', 'ccdc', 'ddcc']
        }],
        level2: [{
          matching: ['aabaa', 'bbbab', 'abbb', 'ab', 'babaa', 'aadaa', 'dddad', 'addd', 'ad', 'dadaa', 'bbcbb', 'cccbc', 'bccc', 'bc', 'cbcbb', 'ccdcc', 'dddcd', 'cddd', 'cd', 'dcdcc'],
          nonMatching: ['bbbba', 'bba', 'bbb', 'baaa', 'dddda', 'dda', 'ddd', 'daaa', 'ccccb', 'ccb', 'ccc', 'cbbb', 'ddddc', 'ddc', 'ddd', 'dccc', 'aaaca', 'ddbbd', 'abc', 'dbaac']
        }]
      }
    },
    all: {
      modes: [{
        key: 'cooperative',
        note: 'Different regEx, one score, timer, four level.'
      }]
      /* endCondition: [{
        key: "level",
        note: "Ends the game, if the level is finished.",
      }, {
        key: "timer",
        note: "End the game, if the time is up."
      }], */
    }
  }

  return settings
};
