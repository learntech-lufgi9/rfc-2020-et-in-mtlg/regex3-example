/**
 * @Author: thiemo
 * @Date:   2018-08-24T10:08:36+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-09-03T19:17:15+02:00
 */

/**
 * Startlevel that selects the right game to start
 * @return 1 if game starts for the first time, else 0
 */
var gameSelect = function () { // eslint-disable-line no-unused-vars
  console.log('Dummy Level to get parallel gamemodes and settings working with lc.')
  // MTLG.clearBackground()
  // MTLG.setBackgroundColor('black')

  xAPI.init()
  for (let i = 0; i < MTLG.getPlayerNumber(); i++) {
    xAPI.startSession(MTLG.getPlayerName(i))
  }

  let game = MTLG.getSettings().default.game

  switch (game) {
    case 'RegEx':
      // load gamemode settings
      MTLG.loadSettings(getRegExSettings())
      break
    case 'Word Game':
      MTLG.loadSettings(getWordSettings())
      break
    case 'Animal Game':
      MTLG.loadSettings(getAnimalSettings())
      break
    case 'Chemie Game':
      MTLG.loadSettings(getChemieSettings())
      break

    default:
      MTLG.lc.goToMenu()
  }

  if (game) {
    MTLG.lc.levelFinished({
      nextLevel: 1
    })
  }
}

var gameSelectCheck = function (gameState) { // eslint-disable-line no-unused-vars
  if (!gameState) {
    return 1
  }
  if (!gameState.nextLevel) {
    return 1
  }
  return 0
}

/// //////////////////////////////////

var gameMenu = function (gameState) { // eslint-disable-line no-unused-vars
  // MTLG.getStage().removeAllEventListeners();
  // MTLG.getStage().removeAllChildren();
  // localStage = MTLG.getStage();
  // let localStage = MTLG.getStageContainer();

  // MTLG.getStage().addChild(localStage);

  drawGameMenu()
}

/**
 * Function that checks if Feld01 should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
var checkLevel1 = function (gameState) { // eslint-disable-line no-unused-vars
  if (!gameState) {
    return 0 // assuming we should not start
  }
  if (!gameState.nextLevel) {
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 1) {
    return 1
  } else {
    return 0
  }
}

var templateGame = function (gameState) { // eslint-disable-line no-unused-vars
  MTLG.getStageContainer().removeAllEventListeners()
  MTLG.getStageContainer().removeAllChildren()
  // localStage = MTLG.getStage();
  let localStage = MTLG.getStageContainer()

  // initialize feedback controller
  fbC.init()
  // console.log('Initialize LCDM controller')
  // lcdmC.init()

  // initialize MVP
  if (!gameState.level) {
    let _model = new Model(MTLG.getSettings().default.verifyFunction)
    let _view = new View(localStage)
    Presenter.init(_model, _view, MTLG.getSettings().default)
  } else {
    console.log('only view')
    let _view = new View(localStage)
    Presenter.newGame(_view, MTLG.getSettings().default)
  }
}

/**
 * Function that checks if Feld01 should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
function checkLevel2 (gameState) { // eslint-disable-line no-unused-vars
  if (!gameState) {
    return 0 // assuming we should not start
  }
  if (!gameState.nextLevel) {
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 2) {
    return 1
  } else {
    return 0
  }
}
