
function getRegExSettings () { // eslint-disable-line no-unused-vars
  let verifyFunction = function (word, pattern) {
    return pattern.test(word)
  }

  let regExToString = function (regEx) {
    return regEx.toString().slice(3, regEx.length - 4)
  }

  let settings = {
    default: {
      gameName: 'Regular Expressions - RoadShow',
      bgImage: 'background/RegEx.jpg',
      verifyFunction: verifyFunction,
      regExToString: regExToString,
      gameMode: 'cooperative',
      useConstant: true, // Use constant regEx/word or generate randomly
      nbrWords: 20,
      alphabet: ['a', 'b'],
      endCondition: 'level',
      timerMaxTime: 1 * 90 * 1000, // Per Game max time in milli-seconds
      delayWords: 0, // delay till the words will appear
      maxLevel: 15,
      tutMaxWords: 15,
      plusScore: 2,
      minusScore: -1,
      constantRegEx: {
        level0: [/^(a*|bb)$/i, /^(ab|b*)$/i],
        level1: [/^((a|ab)*)$/i, /^((a|ba)*)$/i],
        level2: [/^((ba|ab)(b|a)*|b*(aa)*)$/i, /^(bab*(b|a)b*|a*b*)$/i]
      },
      constantWords: {
        level0: [{
          matching: ['a', 'aa', 'bb', 'aaa'],
          nonMatching: ['ab', 'ba', 'b', 'bba', 'aab', 'aba']
        }, {
          matching: ['b', 'ab', 'bb', 'bbb'],
          nonMatching: ['ba', 'aa', 'a', 'bba', 'aab', 'aba']
        }],
        level1: [{
          matching: ['a', 'ab', 'aba', 'aab', 'aaab', 'ababa', 'aaa'],
          nonMatching: ['ba', 'abab', 'b', 'bba', 'bb']
        }, {
          matching: ['a', 'ba', 'aba', 'baa', 'aaba', 'ababa', 'aaa'],
          nonMatching: ['ab', 'abab', 'b', 'bba', 'bb']
        }],
        level2: [{
          matching: ['babbb', 'abab', 'abbaba', 'baaba', 'bbaa', 'aaaa', 'bbbaa'],
          nonMatching: ['bbbbb', 'aabab', 'aaab', 'bbba', 'aab']
        }, {
          matching: ['aaab', 'abbb', 'bbbb', 'baa', 'babbab', 'bab'],
          nonMatching: ['aaaba', 'ba', 'babaa', 'aaba', 'babbaba']
        }]
      },
      /*
      tutorial mode (demo)
      */
      tutorialRegEx: {
        level0: [/^(a*|bb)$/i]
      },
      tutorialWords: {
        level0: [{
          matching: ['a', 'aa', 'bb', 'aaa'],
          nonMatching: ['ab', 'ba', 'b', 'bba', 'aab', 'aba']
        }]
      },
      /*
      collaborative mode (learning)
      */
      collaborativeRegEx: {
        level0: [/^(a|b)$/i],
        level1: [/^(a*|bb)$/i],
        level2: [/^((a|ab)*)$/i],
        level3: [/^((ba|ab)(b|a)*|b*(aa)*)$/i]
      },
      collaborativeWords: {
        level0: [{
          matching: ['a', 'b'],
          nonMatching: ['aa', 'ab', 'ba', 'aba', 'bba', 'aab', 'bb']
        }],
        level1: [{
          matching: ['a', 'aa', 'aaa', 'aaaa', 'bb'],
          nonMatching: ['b', 'ab', 'ba', 'bbb', 'aab', 'aba', 'baa', 'bba', 'bab', 'abb', 'bbb']
        }],
        level2: [{
          matching: ['a', 'ab', 'aba', 'aab', 'aaab', 'ababa', 'aaa'],
          nonMatching: ['ba', 'abab', 'b', 'bba', 'bb']
        }],
        level3: [{
          matching: ['babbb', 'abab', 'abbaba', 'baaba', 'bbaa', 'aaaa', 'bbbaa'],
          nonMatching: ['bbbbb', 'aabab', 'aaab', 'bbba', 'aab']
        }]
      },
      /*
      cooperative mode (together)
      */
      cooperativeRegEx: {
        level0: [
          [/^((ab)*)$/i, /^((cd)*)$/i, /^((bc)*)$/i, /^((ad)*)$/i]
        ], // Array of arrays, inner arrays have a regEx for each of the 4 players
        level1: [
          [/^((a|ab)*)$/i, /^((b|ba)*)$/i, /^((c|cd)*)$/i, /^((d|dc)*)$/i]
        ], // Array of arrays, inner arrays have a regEx for each of the 4 players
        level2: [
          [/^((a|b)*ab(a|b)*)$/i, /^((a|d)*ad(a|d)*)$/i, /^((b|c)*bc(b|c)*)$/i, /^((c|d)*cd(c|d)*)$/i]
        ] // Array of arrays, inner arrays have a regEx for each of the 4 players
      },
      cooperativeWords: {
        level0: [{
          matching: ['ab', 'abab', 'ababab', 'cd', 'cdcd', 'cdcdcd', 'bc', 'bcbc', 'bcbcbc', 'ad', 'adad', 'adadad'],
          nonMatching: ['ba', 'aba', 'ababa', 'abaab', 'dc', 'cdc', 'cdcdc', 'cdccd', 'cb', 'cbc', 'cbcbc', 'cbccb', 'da', 'ada', 'adada', 'adaad']
        }],
        level1: [{
          matching: ['a', 'ab', 'aaa', 'aba', 'aaabaa', 'b', 'ba', 'bbb', 'bab', 'bbbabb', 'c', 'cd', 'ccc', 'cdc', 'cccdcc', 'd', 'dc', 'ddd', 'dcd', 'dddcdd'],
          nonMatching: ['ababbb', 'ababb', 'abb', 'bbab', 'aabb', 'babaaa', 'babaa', 'baa', 'aaba', 'bbaa', 'cdcddd', 'cdcdd', 'cdd', 'ddcd', 'ccdd', 'dcdccc', 'dcdcc', 'dcc', 'ccdc', 'ddcc']
        }],
        level2: [{
          matching: ['aabaa', 'bbbab', 'abbb', 'ab', 'babaa', 'aadaa', 'dddad', 'addd', 'ad', 'dadaa', 'bbcbb', 'cccbc', 'bccc', 'bc', 'cbcbb', 'ccdcc', 'dddcd', 'cddd', 'cd', 'dcdcc'],
          nonMatching: ['bbbba', 'bba', 'bbb', 'baaa', 'dddda', 'dda', 'ddd', 'daaa', 'ccccb', 'ccb', 'ccc', 'cbbb', 'ddddc', 'ddc', 'ddd', 'dccc', 'aaaca', 'ddbbd', 'abc', 'dbaac']
        }]
      }
    },

    design: {
      words: {
        font: 'Arial',
        fontType: '',
        fontColor: 'white', // 'rgb(51, 204, 255)',
        background: '#087faa'
      },
      pattern: {
        font: 'Arial',
        fontType: '',
        fontColor: '#262626', // rgb(243,242,242)',
        fontBackground: 'rgba(51, 204, 255, 0.9)',
        fontAlign: 'center',
        background: 'rgba(51, 204, 255, 0.2)'
      }
    },

    all: {
      modes: [{
        key: 'tutorial',
        note: 'Same RegEx, different scores, no timer, one level.'
      },
      {
        key: 'collaborative',
        note: 'Same regEx, one score, no timer, two level.'
      },
      {
        key: 'cooperative',
        note: 'Different regEx, one score, timer, four level.'
      }
        /* {
          key: "competitive",
          note: "Same regEx, different scores, timer."
        } */
      ],
      endCondition: [{
        key: 'level',
        note: 'Ends the game, if the level is finished.'
      }, {
        key: 'timer',
        note: 'End the game, if the time is up.'
      }]
    }
  }

  return settings
};
