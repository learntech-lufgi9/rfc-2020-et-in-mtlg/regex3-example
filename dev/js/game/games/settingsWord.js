/**
 * @Author: thiemo
 * @Date:   2017-11-17T12:52:37+01:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-08-29T11:12:19+02:00
 */

function getWordSettings () { // eslint-disable-line no-unused-vars
  let pattern = {
    level0: [
      ['Verben', 'Adjektive', 'Nomen', 'Konjunktionen']
    ]
  }

  let words = {
    level0: [{
      matching: ['laufen', 'reden', 'fahren', 'singen', 'tanzen', 'schnell', 'laut', 'klein', 'gelb', 'lustig', 'süß', 'Hai', 'Haus', 'Obst', 'Wiese', 'Stein', 'und', 'oder', 'als', 'wie', 'ob', 'dass'],
      nonMatching: ['hinter', 'auf', 'bei', 'für', 'wegen', 'zu', 'an', 'über', 'unter', 'ach', 'nanu', 'oh', 'pfui']
    }]
  }

  let verifyFunction = function (word, pattern) {
    let _solution = [{
      pattern: 'Verben',
      words: ['laufen', 'reden', 'fahren', 'singen', 'tanzen']
    }, {
      pattern: 'Adjektive',
      words: ['schnell', 'laut', 'klein', 'gelb', 'lustig', 'süß']
    }, {
      pattern: 'Nomen',
      words: ['Hai', 'Haus', 'Obst', 'Wiese', 'Stein']
    }, {
      pattern: 'Konjunktionen',
      words: ['und', 'oder', 'als', 'wie', 'ob', 'dass']
    }]

    function search (el) {
      if (el.pattern === pattern) {
        return el.words.includes(word)
      }
      return false
    }

    if (_solution.find(search) !== undefined) {
      return true
    }

    return false
  }

  let regExToString = function (regEx) {
    return regEx
  }

  let settings = {
    default: {
      gameName: 'Words - RoadShow',
      bgImage: 'background/Words.jpg',
      bgPath: '',
      regExToString: regExToString,
      verifyFunction: verifyFunction,
      gameMode: 'cooperative',
      useConstant: true, // Use constant regEx/word or generate randomly
      nbrWords: 25,
      timerMaxTime: 1 * 90 * 1000, // Per Game max time in milli-seconds
      delayWords: 0, // delay till the words will appear
      plusScore: 2,
      minusScore: -1,
      alphabet: ['words'],
      cooperativeRegEx: pattern,
      cooperativeWords: words,
      wordView: 'words'
    },
    design: {
      words: {
        font: 'Comic Sans MS',
        fontType: 'italic',
        fontColor: 'blue',
        background: 'white'
      },
      pattern: {
        font: 'Arial',
        fontType: 'italic',
        fontColor: 'rgb(243,242,242)',
        fontBackground: 'rgba(0, 0, 0, 0.8)',
        fontAlign: 'center',
        background: 'rgba(0, 0, 0, 0.3)'
      }
    },
    all: {
      modes: [{
        key: 'cooperative',
        note: 'Different regEx, one score, timer, four level.'
      }]
    }
  }

  return settings
}
