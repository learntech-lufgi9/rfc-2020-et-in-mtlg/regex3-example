/**
 * @Author: thiemo
 * @Date:   2017-11-17T12:52:37+01:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-08-29T11:12:19+02:00
 */

function getAnimalSettings () { // eslint-disable-line no-unused-vars
  let pattern = {
    level0: [
      ['Vögel', 'Insekten', 'Fische', 'Landtiere']
    ]
  }

  let words = {
    level0: [{
      matching: ['Ente', 'Koala', 'Ameise', 'Hai', 'Biene', 'Adler', 'Maus'],
      nonMatching: ['Auto', 'Bahn', 'Boot', 'Baum', 'Marmelade']
    }],
    level1: [{
      matching: ['Auto', 'Bahn', 'Boot', 'Baum', 'Marmelade'],
      nonMatching: ['Ente', 'Koala', 'Ameise', 'Hai', 'Biene', 'Adler', 'Maus']
    }]
  }

  let verifyFunction = function (word, pattern) {
    let _solution = [{
      pattern: 'Vögel',
      words: ['Ente', 'Adler']
    }, {
      pattern: 'Insekten',
      words: ['Ameise', 'Biene']
    }, {
      pattern: 'Fische',
      words: ['Hai']
    }, {
      pattern: 'Landtiere',
      words: ['Koala', 'Maus']
    }, {
      pattern: 'Pflanze',
      words: ['Baum']
    }, {
      pattern: 'Zug',
      words: ['Bahn']
    }, {
      pattern: 'Boot',
      words: ['Boot']
    }, {
      pattern: 'Essen',
      words: ['Marmelade']
    }]

    function search (el) {
      if (el.pattern === pattern) {
        return el.words.includes(word)
      }
      return false
    }

    if (_solution.find(search) !== undefined) {
      return true
    }

    return false
  }

  let regExToString = function (regEx) {
    return regEx
  }

  let settings = {
    default: {
      gameName: 'Animals - RoadShow',
      bgImage: 'background/Animals.jpg',
      bgPath: '',
      regExToString: regExToString,
      verifyFunction: verifyFunction,
      gameMode: 'cooperative',
      useConstant: true, // Use constant regEx/word or generate randomly
      nbrWords: 25,
      timerMaxTime: 1 * 90 * 1000, // Per Game max time in milli-seconds
      delayWords: 0, // delay till the words will appear
      plusScore: 2,
      minusScore: -1,
      alphabet: ['animals'],
      cooperativeRegEx: pattern,
      cooperativeWords: words,
      wordView: 'pictures'
    },
    design: {
      pattern: {
        font: 'Arial',
        fontType: '',
        fontColor: 'white',
        fontBackground: 'rgba(38, 38, 38, 0.7)',
        fontAlign: 'center',
        background: 'rgba(38, 38, 38, 0.2)'
      }
    },
    all: {
      modes: [{
        key: 'cooperative',
        note: 'Different regEx, one score, timer, four level.'
      }]
    }
  }

  return settings
}
