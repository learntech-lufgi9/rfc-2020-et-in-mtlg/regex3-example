function getChemieSettings () { // eslint-disable-line no-unused-vars
  let pattern = {
    level0: [
      ['Aldehyde', 'Alkene', 'Ether', 'Ketone']
    ],
    level1: [
      ['Alkine', 'Alkohole', 'Carbonsäuren', 'Ester']
    ]
  }; let words = {
    level0: [{
      matching: ['butanal', 'cyclohexancarbaldehyd', 'formaldehyd', 'propanal', '3-methyl-1-penten', 'cyclopenten', 'ethen', 'propen', '2-methoxypropan', 'dimethylether', 'methylethylether', 'oxycyclohexan', '2-pentanon', 'aceton', 'acetophenon', 'benzophenon'],
      nonMatching: ['3-hexin', 'cyclopentin', 'ethin', 'propin', '3-octanol', 'cyclohexanol', 'ethanol', 'ethylenglycol', 'bernsteinsaeure', 'buttersaeure', 'essigsaeure', 'oxalsaeure', 'butylacetat', 'ethylacetat', 'gamma-butyrolacton', 'methylacetat']
    }],

    level1: [{
      nonMatching: ['butanal', 'cyclohexancarbaldehyd', 'formaldehyd', 'propanal', '3-methyl-1-penten', 'cyclopenten', 'ethen', 'propen', '2-methoxypropan', 'dimethylether', 'methylethylether', 'oxycyclohexan', '2-pentanon', 'aceton', 'acetophenon', 'benzophenon'],
      matching: ['3-hexin', 'cyclopentin', 'ethin', 'propin', '3-octanol', 'cyclohexanol', 'ethanol', 'ethylenglycol', 'bernsteinsaeure', 'buttersaeure', 'essigsaeure', 'oxalsaeure', 'butylacetat', 'ethylacetat', 'gamma-butyrolacton', 'methylacetat']
    }]

  }; let verifyFunction = function (word, pattern) {
    let _solution = [{
      pattern: 'Aldehyde',
      words: ['butanal', 'cyclohexancarbaldehyd', 'formaldehyd', 'propanal']
    }, {
      pattern: 'Alkene',
      words: ['3-methyl-1-penten', 'cyclopenten', 'ethen', 'propen']
    }, {
      pattern: 'Ether',
      words: ['2-methoxypropan', 'dimethylether', 'methylethylether', 'oxycyclohexan']
    }, {
      pattern: 'Ketone',
      words: ['2-pentanon', 'aceton', 'acetophenon', 'benzophenon']
    }, {
      pattern: 'Alkine',
      words: ['3-hexin', 'cyclopentin', 'ethin', 'propin']
    }, {
      pattern: 'Alkohole',
      words: ['3-octanol', 'cyclohexanol', 'ethanol', 'ethylenglycol']
    }, {
      pattern: 'Carbonsäuren',
      words: ['bernsteinsaeure', 'buttersaeure', 'essigsaeure', 'oxalsaeure']
    }, {
      pattern: 'Ester',
      words: ['butylacetat', 'ethylacetat', 'gamma-butyrolacton', 'methylacetat']
    }]; function search (el) {
      if (el.pattern === pattern) {
        return el.words.includes(word)
      }

      return false
    } if (_solution.find(search) !== undefined) {
      return true
    } return false
  }

  let regExToString = function (regEx) {
    return regEx
  }

  let settings = {
    default: {
      gameName: 'Chemistry - RoadShow',
      bgImage: 'background/Chemie.jpg',
      bgPath: 'chemie/',
      regExToString: regExToString,
      verifyFunction: verifyFunction,
      gameMode: 'cooperative',
      useConstant: true, // Use constant regEx/word or generate randomly
      nbrWords: 20,
      timerMaxTime: 1 * 90 * 1000, // Per Game max time in milli-seconds
      delayWords: 0, // delay till the words will appear
      plusScore: 2,
      minusScore: -1,
      alphabet: ['molecules'],
      cooperativeRegEx: pattern,
      cooperativeWords: words,
      wordView: 'pictures'
    },
    all: {
      modes: [{
        key: 'cooperative',
        note: 'Different regEx, one score, timer, four level.'
      }]
    }
  }

  return settings
};
