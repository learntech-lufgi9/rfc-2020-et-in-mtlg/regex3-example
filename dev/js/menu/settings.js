/**
 * @Author: thiemo
 * @Date:   2018-08-23T12:15:29+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-09-03T19:20:25+02:00
 */
var drawSettings = function (gameState) { // eslint-disable-line no-unused-vars
  let stage = MTLG.getStageContainer()

  // Draw the main menu
  // Text
  var template = 'Settings - Roadshow' // l("choose_mode");
  var variableTextSize = 50
  var title = new createjs.Text(template, 'bold ' + variableTextSize + 'px Arial', 'black')
  title.regX = title.getBounds().width / 2
  title.regY = title.getBounds().height / 2
  title.x = MTLG.getOptions().width / 2
  title.y = 150 // options.height / 4;
  title.textBaseline = 'alphabetic'

  // selectionContainer for meta settings
  let selectionMetaContainer = new createjs.Container()
  selectionMetaContainer.regX = 0
  selectionMetaContainer.regY = 0
  selectionMetaContainer.x = 200 // MTLG.getOptions().width / 2 - (1380 + 150) / 2;
  selectionMetaContainer.y = 250

  let dataLogBug = false
  if (MTLG.getSettings().default['dataLogging'] === true) {
    MTLG.getSettings().default['dataLogging'] = 'on'
    dataLogBug = true
  }
  if (MTLG.getSettings().default['dataLogging'] === false) {
    MTLG.getSettings().default['dataLogging'] = 'off'
    dataLogBug = false
  }

  // data logging
  gameUtils.buttonsSelectionGroup({
    cb: function () {
      if (this.cbvar === 'on') {
        MTLG.loadSettings({
          default: {
            dataLogging: true
          }
        })
      } else {
        MTLG.loadSettings({
          default: {
            dataLogging: false
          }
        })
      }
      this.setColor('rgba(0, 155, 0, 0.7)')
      this.resetGroupColor()
      this.showNote()
    },
    cbvar: 'value',
    delta: 20,
    key: 'dataLogging',
    default: MTLG.getSettings().default['dataLogging'],
    menuColor: 'rgb(0, 0, 0)',
    menuTextColor: 'rgb(255, 255, 255)'
  }, MTLG.getSettings().all.dataLogging, {
    x: 0,
    y: 0
  }, selectionMetaContainer)

  // Failure buttonsSelectionGroup for true-false-switch
  MTLG.loadSettings({
    default: {
      dataLogging: dataLogBug
    }
  })

  gameUtils.buttonNbrGroup({
    delta: 20,
    menuColor: 'rgb(0, 0, 0)',
    menuTextColor: 'rgb(255, 255, 255)'
  }, {
    key: 'groupNumber',
    keyMin: 'minGroupNumber'
  }, {
    x: 230,
    y: 0
  }, selectionMetaContainer)

  try {
    var sessionButton = new gameUtils.Button({
      text: MTLG.l('New SessionID'),
      place: {
        x: 460,
        y: 0
      },
      cb: generateSessionID
      // center: true
    }, selectionMetaContainer)
  } catch (e) {
    console.warn(sessionButton)
  }

  // Gamemodes
  gameUtils.buttonsSelectionGroup({
    menuColor: 'rgb(0, 0, 0)',
    menuTextColor: 'rgb(255, 255, 255)',
    cb: function () {
      MTLG.loadSettings({
        default: {
          wordAlignement: this.cbvar
        }
      })
      this.setColor('rgba(0, 155, 0, 0.7)')
      this.resetGroupColor()
      this.showNote()
    },
    cbvar: 'value',
    delta: 20,
    key: 'wordAlignement',
    default: MTLG.getSettings().default['wordAlignement']
  }, MTLG.getSettings().all.wordAlignement, {
    x: 690,
    y: 0
  }, selectionMetaContainer)

  let startContainer = new createjs.Container()
  startContainer.regX = 0
  startContainer.regY = 0
  startContainer.x = MTLG.getOptions().width / 2
  startContainer.y = 800

  let sessionIDText = new gameUtils.Button({
    text: 'SessionID: ' + sessionId,
    place: {
      x: 0,
      y: 200
    },
    // cb: generateSessionID,
    center: true,
    color: 'rgba(0, 0, 0, 0.85)',
    textcolor: 'rgb(255, 255, 255)',
    textstyle: 'bold 48px Arial',
    textSize: true
  }, startContainer)

  try {
    var menuButton = new gameUtils.Button({
      text: MTLG.l('back'),
      place: {
        x: -75,
        y: 50
      },
      cb: MTLG.lc.goToMenu
    // center: true
    }, startContainer)
  } catch (e) {
    console.warn(menuButton)
  }

  // Background
  // MTLG.setBackgroundColor("blue");
  // https://pixabay.com/de/brown-nahaufnahme-hd-wallpaper-1866661/
  // MTLG.setBackgroundImage("background/brown-1866661_1920.jpg"); // pixabay
  MTLG.setBackgroundImageFill('background/brown-1866661.jpg') // pixabay

  stage.addChild(title)

  stage.addChild(selectionMetaContainer)
  stage.addChild(startContainer)

  /// ////////////////////////////////////////////////////////////////////////////
  // helper

  function generateSessionID () {
    let date = Date.now()
    sessionId = hashCode('' + date)
    sessionIDText.c.setText('SessionID: ' + sessionId)

    function hashCode (inputStr) {
      let hash = 0
      if (inputStr.length === 0) {
        return hash
      }
      for (var i = 0; i < inputStr.length; i++) {
        let char = inputStr.charCodeAt(i)
        hash = ((hash << 5) - hash) + char
        hash = hash & hash // Convert to 32bit integer
      }
      if (hash < 1) {
        hash = hash * -1
      }
      // (hash < 1) ? hash = hash * -1 : true
      return hash
    }
  }
}

/**
  * Function that checks if Feld01 should be the next level
  * @param the current game state (an object containing only nextLevel in this case)
  * @return a number in [0,1] that represents that possibility.
  */
var checkSettings = function (gameState) { // eslint-disable-line no-unused-vars
  if (!gameState) {
    console.log('gameState undefined')
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 0.5) {
    return 1
  } else {
    return 0
  }
}
