
var drawMainMenu = function () { // eslint-disable-line no-unused-vars
  var stage = MTLG.getStageContainer()

  MTLG.menu.start()

  // parameter
  let mKeys = []
  for (let i = 0; i < MTLG.getSettings().all.games.length; i++) {
    mKeys.push(MTLG.getSettings().all.games[i].key)
  }

  let _circleMenu = gameUtils.circleMenu({
    mKeys,
    mText: '24px Arial',
    mTextColor: 'rgb(230, 228, 205)',
    mColor: ['rgba(119, 249, 5, 0.6)', 'rgba(249, 71, 5, 0.6)', 'rgba(20, 83, 246, 0.6)', 'rgba(194, 20, 246, 0.6)'],
    mBG: [{ src: 'img/background/Animals.jpg' }, { src: 'img/background/Words.jpg', tx: 3250 * 0.8, ty: 0, scale: 0.8 }, { src: 'img/background/RegEx.jpg' }, { src: 'img/background/Chemie.jpg', tx: 0, ty: 30, scale: 0.7 }],
    mPlBG: [{ src: 'img/background/Animals.jpg', tx: 100, ty: 100, scale: 0.1 }, { src: 'img/background/Words.jpg', tx: 100, ty: 100, scale: 0.3 }, { src: 'img/background/RegEx.jpg', tx: 500, ty: 500, scale: 0.3 }, { src: 'img/background/Chemie.jpg', tx: 100, ty: 100, scale: 0.3 }],
    shadowColor: '#000000'
    // cBG: 'img/games/Uhrzeiger2.png'
  })
  _circleMenu.addDragFeedback()
  stage.addChild(_circleMenu)

  // create sessionId
  let date = Date.now()
  sessionId = hashCode('' + date)

  // sessionId
  let startContainer = new createjs.Container()
  startContainer.addChild(new gameUtils.Button({
    text: 'SessionID: ' + sessionId,
    place: {
      x: 0,
      y: 0
    },
    center: true,
    color: 'rgba(0, 0, 0, 0.85)',
    textcolor: 'rgb(255, 255, 255)',
    textstyle: 'bold 36px Arial',
    textSize: true
  }))

  startContainer.regX = startContainer.regY = 0
  startContainer.x = MTLG.getOptions().width - 50
  startContainer.y = MTLG.getOptions().height / 2
  startContainer.rotation = 90

  let startContainerClone = startContainer.clone(true)
  startContainerClone.rotation = -90
  startContainerClone.x = 50

  // var chooser = MTLG.getStageContainer().getChildAt(0).getChildAt(1).getChildAt(0)
  //
  // let coords = chooser.localToLocal(chooser.getBounds().height / 2, 0, MTLG.getStageContainer())
  //
  // var movetotarget = MTLG.utils.fbm.createContent('movetotarget')
  // movetotarget.pointCounter = 3
  // movetotarget.pointStartX = coords.x / MTLG.getOptions().width // (chooser.parent.parent.x + 450) / MTLG.getOptions().width
  // movetotarget.pointStartY = coords.y / MTLG.getOptions().height// chooser.parent.parent.y / MTLG.getOptions().height
  // movetotarget.pointEndX = coords.x / MTLG.getOptions().width// (chooser.parent.parent.x + 450) / MTLG.getOptions().width
  // movetotarget.pointEndY = (coords.y + 100) / MTLG.getOptions().height// (chooser.parent.parent.y + 100) / MTLG.getOptions().height
  // movetotarget.pointRotation = 0
  // movetotarget.pointSpeed = 60
  // movetotarget.pointSize = 90
  //
  // MTLG.utils.fbm.giveFeedback(chooser, 'movetotarget', 'overallTiming', 'neutralStyle')

  MTLG.clearBackground()
  MTLG.setBackgroundImageFill('background/brown-1866661.jpg') // pixabay

  stage.addChild(startContainer, startContainerClone)

  //Eyetracking init
  MTLG.eyetracking.initialize();

  // log in demo player if not yet present
  if (!MTLG.getPlayerNumber()) {
      console.log("Logging in");
      MTLG.lc.goToLogin(); //Leave Main Menu and go to login

  }else{
      MTLG.eyetracking.initPlayers();
  }

  MTLG.eyetracking.visualizeGazeOfAllPlayers();
}
