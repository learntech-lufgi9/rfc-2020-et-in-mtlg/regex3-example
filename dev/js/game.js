/**
 * @Author: thiemo
 * @Date:   2017-10-17T11:41:05+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-08-29T09:56:59+02:00
 */

/*
 * These variables should always be present and used if necessary.
 */
var // options, // game options read from manifest files
  // gameData, // game state, level info...
  // areas, // Information about user areas.
  // sharedArea, // Free area left
  // players, // An array containing information about the players.
  // firstRun, // A bool remembering if this is the first time the game is run
  // A string that is unique to this session
  sessionId, // eslint-disable-line no-unused-vars
  // The current version of settings (if different sets are available)
  settingsVersion // eslint-disable-line no-unused-vars

/*
 * These variables are game specific.
 */
// var readyCounter // Counts the number of players that are ready

// It is useful to redefine MTLG.lang.getString
// Write l(key) to get the corresponding string.
// The lang module is loaded after spiel, so the var is defined in init.
// var l

// expose framework for debugging
window.MTLG = MTLG
window.window.createjs = createjs
// ignite the framework
document.addEventListener('DOMContentLoaded', MTLG.init)

// add moduls
require('mtlg-modul-menu')
require('mtlg-moduls-utilities')
require('mtlg-module-eyetracking') //Why not automaticly?
require('mtlg-modul-lightparse')
require('mtlg-modul-datacollector')
require('mtlg-modul-tabulaevents')
// require('mtlg-modul-tangibles')
require('mtlg-module-dd')
require('mtlg-module-remoteteacher')

// load configuration
require('../manifest/device.config.js')
require('../manifest/game.config.js')
require('../manifest/game.settings.js')

// load translations
require('../lang/lang.js')

/**
 * Init function for the game.
 * @params options: The options parsed from the manifest files/ Defaults.
 */
var initGame = function (pOptions) {
  // Initialize game
  // Set default var values
  setupVars(pOptions)

  // Define l as abbreveation  for translations.
  // l = MTLG.lang.getString

  // Initialize levels and Menus:
  // The MTLG framework uses the lifecycle manager (lc) to handle level progression,
  // resetting levels and to define a starting point for the game.
  // Use the functions registerMenu and registerLevel to define your levels.
  // The starting point of the game will be the registered menu, if one is present,
  // or the game with the highest value when passed the empty game state.
  // Register Menu
  MTLG.lc.registerMenu(drawMainMenu)

  // Register levels
  // Levels consist of two parts: the first parameter is the entrance function,
  // that sets the level up and renders it. The second parameter as a check function,
  // that receives the current gamestate as paramter and returns a value between 0 and 1.
  // The lifecycle manager starts the level with the highest return value in the next step.
  MTLG.lc.registerLevel(gameSelect, gameSelectCheck)

  // register settings
  MTLG.lc.registerLevel(drawSettings, checkSettings) // settings

  // register games
  MTLG.lc.registerLevel(gameMenu, checkLevel1)
  MTLG.lc.registerLevel(templateGame, checkLevel2)

  MTLG.loadSettings({
    default: {
      skipMenu: true
    }
  })
  MTLG.menu.addSubMenu(function () {
    if (MTLG.getSettings().default.skipMenu === true) {
      MTLG.loadSettings({
        default: {
          skipMenu: false
        }
      })
    } else {
      MTLG.loadSettings({
        default: {
          skipMenu: true
        }
      })
    }
  })

  // Init is done
  console.log('Game Init function called')

  // Create and join room for remoteteacher module
  // TODO: via timeout is bad, a hook should be provided by ddd

  // disable RT
  if(!window.location.search.includes("pupil") && !window.location.search.includes("teacher"))
    return;

  setTimeout(() => {
      let ensureRoomExists = new Promise((fulfill, reject) => {
        MTLG.distributedDisplays.rooms.getAllRooms(rooms => {
          if(rooms['RT_tabula']) return fulfill()
          MTLG.distributedDisplays.rooms.createRoom('RT_tabula', result => {
            if(result && result.success) fulfill()
            else reject(result.reason)
          })
        })
      })
      ensureRoomExists.catch(err => console.warn('failed to create classroom: '+err)).then(() => {
        // join room
        MTLG.distributedDisplays.rooms.joinRoom('RT_tabula', result => {
          if (result && result.success) {
            console.log('successfully joined classroom')
            // start sharing as teacher or pupil
            if(window.location.search.includes("teacher")) {
              MTLG.remoteteacher.teacherView.init('RT_tabula')
              MTLG.lc.registerMenu(MTLG.remoteteacher.teacherView.draw)
              MTLG.lc.goToMenu()
              // MTLG.remoteteacher.teacherView.draw()
            } else {
              MTLG.remoteteacher.playerView.init('RT_tabula')
              MTLG.remoteteacher.playerView.forSharing(MTLG.getStage())
            }
          } else console.warn('failed to join classroom: '+result.reason)
        })
      })
  }, 1000)
}

// Register Init function with the MTLG framework
// The function passed to addGameInit will be used to initialize the game.
// Use the initialization to register levels and menus.
MTLG.addGameInit(initGame)

/*
 * sets parameters and optionally does further instantiations
 *
 * pOptions contain:
 * language: game's language
 * width: width of playing field in browser's full screen mode
 * height: height of playing field in browser's full screen mode
 * countdown: timespan that must pass without anything happening, before game requests a restart
 * fps: preset FPS for TweenJS
 */
var setupVars = function (pOptions) {
  // stage = MTLG.getStage();
  // options = pOptions
  // gameData = {}
  // readyCounter = 0
  // areas = []
  // sharedArea = {}
  // players = [] // This can be populated in the first level after MTLG.goToLogin was called.
  // firstRun = true
  settingsVersion = 0

  // Create sessionID
  // let date = Date.now()
  // sessionId = hashCode('' + date)
}

/**
 * Taken from https://stackoverflow.com/questions/6122571/simple-non-secure-hash-function-for-javascript
 */
var hashCode = function (inputStr) {
  let hash = 0
  if (inputStr.length === 0) {
    return hash
  }
  for (var i = 0; i < inputStr.length; i++) {
    let char = inputStr.charCodeAt(i)
    hash = ((hash << 5) - hash) + char
    hash = hash & hash // Convert to 32bit integer
  }
  if (hash < 1) {
    hash = hash * -1
  }
  return hash
}
